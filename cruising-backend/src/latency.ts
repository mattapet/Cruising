import * as express from 'express';

export function latency(amount: number) {
    return (_: express.Request, __: express.Response, next: express.NextFunction) => {
        setTimeout(() => next(), Math.random() * amount);
    };
}
