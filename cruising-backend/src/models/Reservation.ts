import { Store, Identifiable } from './Store';

export enum ReservationState {
    initial   = 'initial',
    confirmed = 'confirmed',
    fulfilled = 'fulfilled',
    canceled  = 'canceled',
    rejected  = 'rejected'
}

export interface Reservation extends Identifiable {
    state: ReservationState;
    cruiseId: number;
    userId: number;
    confirmerId: number;
}

export class ReservationStore extends Store<Reservation> {
    public getByUserId(userId: number): Reservation[] {
        return this.getAll().filter(
            (item) => item.userId === userId || item.confirmerId === userId
        );
    }

    public static sharedInstance = new ReservationStore();
}

