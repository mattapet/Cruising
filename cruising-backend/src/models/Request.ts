import { Store, Identifiable } from './Store';

export interface Request extends Identifiable {
    userId: number;
    cruiseId: number;
}

export class RequestStore extends Store<Request> {
    public getByCruiseId(cruiseId: number): Request[] {
        return this.getAll().filter((item) => item.cruiseId === cruiseId);
    }

    public getByUserId(userId: number): Request[] {
        return this.getAll().filter((item) => item.userId === userId);
    }

    public static sharedInstance = new RequestStore();
}
