import { Identifiable, Store } from './Store';
import { Request } from './Request';
import { Reservation } from './Reservation';

export interface Cruise extends Identifiable {
    name: string;
    price: number;
    coverImageURL: string;
    description?: string;
    imageURLs: string[];
}

export class CruiseStore extends Store<Cruise> {
    public populateRequests(requests: Request[]): Request[] {
        return requests.map((request) => {
            (request as any).cruise = this.store.get(request.cruiseId);
            return request;
        });
    }

    public populateReservations(reservations: Reservation[]): Reservation[] {
        return reservations.map((reservation) => {
            (reservation as any).cruise = this.store.get(reservation.cruiseId);
            return reservation;
        });
    }

    public static sharedInstance = new CruiseStore();
}

