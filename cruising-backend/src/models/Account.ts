import { Identifiable, Store } from './Store';
import { User } from './User';

export interface Account extends Identifiable {
    user: User;
    password: string;
}

export class AccountStore extends Store<Account> {
    public getByEmail(email: string) {
        const email_ = email.toLowerCase();
        return this.getByCompletion(acc => acc.user.email === email_);
    }

    public authenticate(email: string, password: string): Account | undefined {
        const account = this.getByEmail(email);
        if (account && account.password === password) {
            return account;
        }
        return undefined;
    }

    public static sharedInstance = new AccountStore();
}
