export interface Identifiable {
    id?: number
}

export class Store<T extends Identifiable> {
    protected store: Map<number, T> = new Map();
    protected constructor() {}

    // Get
    public getById(id: number): T | undefined {
        return this.store.get(id);
    }

    public getByIds(ids: number[]): T[] {
        const idsSet = new Set(ids);
        return Array.from(this.store.values())
        .filter((value) => idsSet.has(value.id as number));
    }

    public getByCompletion(completion: (item: T) => boolean): T | undefined {
        for (const [_, value] of this.store) {
            if (completion(value)) {
                return value;
            }
        }
        return undefined;
    }

    public getAll(): T[] {
        return Array.from(this.store.values()) as T[];
    }

    // Insert / Update
    public set(model: T): T {
        if (!model.id) {
            model.id = this.store.size + 1;
        }
        this.store.set(model.id, model);
        return model;
    }

    // Remove
    public removeById(id: number): boolean {
        return this.store.delete(id);
    }
}
