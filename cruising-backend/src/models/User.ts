import { Identifiable, Store } from './Store';
import { Request } from './Request';
import { Reservation } from './Reservation'

export enum Gender {
    male = 'male',
    female = 'female'
}

export interface User extends Identifiable {
    firstName: string
    lastName: string
    email: string
    birthDate: Date
    gender: Gender
    favoriteIds: number[];
}

export class UserStore extends Store<User> {
    public getByEmail(email: string): User | undefined {
        const email_ = email.toLowerCase();
        return this.getByCompletion(user => user.email === email_);
    }

    public populateRequests(requests: Request[]): Request[] {
        return requests.map((request) => {
            (request as any).user = this.store.get(request.userId);
            return request;
        });
    }

    public populateReservations(reservations: Reservation[]): Reservation[] {
        return reservations.map((reservation) => {
            (reservation as any).user = this.store.get(reservation.userId);
            (reservation as any).confirmer = this.store.get(reservation.confirmerId);
            return reservation;
        });
    }

    public static sharedInstance = new UserStore();
}
