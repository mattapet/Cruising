import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import { latency } from './latency';

import './dbSetup';
const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(latency(1000));

import { LoginRouter } from './api/login';
import { CruiseRouter } from './api/cruise';
import { UserRouter } from './api/user';
import { RequestRouter } from './api/request';
import { ReservationRouter } from './api/reservation';
app.use('/api', LoginRouter);
app.use('/api/cruise', CruiseRouter);
app.use('/api/user', UserRouter);
app.use('/api/request', RequestRouter);
app.use('/api/reservation', ReservationRouter);

app.listen(3000);
