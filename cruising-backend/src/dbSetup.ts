import { AccountStore } from './models/Account';
import { Gender, UserStore } from './models/User';
import { CruiseStore } from './models/Cruise';
import { RequestStore } from './models/Request';

(function dbSetup() {
    const profile = {
        id: 1,
        firstName: 'Peter',
        lastName: 'Matta',
        email: 'peter@matta.com',
        birthDate: new Date('1996-05-10'),
        gender: Gender.male,
        favoriteIds: []
    };
    const anotherProfile = {
        id: 2,
        firstName: 'June',
        lastName: 'Moon',
        email: 'june@moon.com',
        birthDate: new Date('1983-11-15'),
        gender: Gender.female,
        favoriteIds: []
    };
    AccountStore.sharedInstance.set({ user: profile, password: '12345' });
    AccountStore.sharedInstance.set({ user: anotherProfile, password: 'password' });
    UserStore.sharedInstance.set(profile);
    UserStore.sharedInstance.set(anotherProfile);

    CruiseStore.sharedInstance.set({
        name: 'Mediterranean Sea',
        price: 2099,
        coverImageURL: 'https://images.dreamlines.de/YYWLcC-J8fbyiWinspbOGKQGLMQ=/580x340/smart/filters:quality(50)/au/files/cruises/areas/greecesantorini12675766m-klein-336419.jpg',
        description: 'Mediterranean Europe is the world\'s top cruising destination and welcomes over 200 million tourists annually. One of the main appeals of the Mediterranean is the climate; the promise of summer sun and long lazy beach days. The beaches of the Mediterrean are understandably a top reason for the fleet of tourists- from the brimming beaches of the Greek isles including Lefkada and Elafonissi Crete, to Spiaggia Rosa in Sardinia and not to mention the Spectacular Spanish coastlines such as Cabo de Gata, Andalucia. There\'s more to the region than the beaches however, and away from the coast one can appreciate the spectacular ancient landscape.',
        imageURLs: [
            'https://images.dreamlines.de/YYWLcC-J8fbyiWinspbOGKQGLMQ=/580x340/smart/filters:quality(50)/au/files/cruises/areas/greecesantorini12675766m-klein-336419.jpg',
            'https://images.dreamlines.de/IRHE0qERcOWRFCedfvQ_sPo97Y0=/580x340/smart/filters:quality(50)/au/files/cruises/areas/seacloudcruisesseacloudontdekdeschoonheidvanspanjemalaga-336417.jpg',
            'https://images.dreamlines.de/GkRSQ2oveKO6Ng9Fr8UwxF7AY4Y=/580x340/smart/filters:quality(50)/au/files/cruises/areas/rom8-336418.jpg',
            'https://images.dreamlines.de/FkPlFVa96Rf5Qm0s_W5ehuND2bQ=/580x340/smart/filters:quality(50)/au/files/cruises/areas/athen17-336416.jpg',
            'https://images.dreamlines.de/YdJGia-1_twGNuQy07ui3WP3sO0=/580x340/smart/filters:quality(50)/au/files/cruises/areas/kreta8jpg-331864.jpg'
        ]
    });
    CruiseStore.sharedInstance.set({
        name: 'Australia',
        price: 2199,
        coverImageURL: 'https://images.dreamlines.de/HfNdRcS7k86PogRNWrqVfj_Q9RE=/580x340/smart/filters:quality(50)/au/files/cruises/areas/mackay2-343348.jpg',
        description: 'A cruise from Australia is the best way to discover the breathtaking coastlines, exciting cities, unique landscapes and vibrant cultures of the Land Down Under. Blessed with an incomparable natural beauty, great weather, the world\'s best beaches and a contagiously relaxed atmosphere, Australia mesmerizes locals and international tourists alike.',
        imageURLs: [
            'https://images.dreamlines.de/HfNdRcS7k86PogRNWrqVfj_Q9RE=/580x340/smart/filters:quality(50)/au/files/cruises/areas/mackay2-343348.jpg',
            'https://images.dreamlines.de/DKG_-_XGAyYO5msAn8ZBL7DODzE=/580x340/smart/filters:quality(50)/au/files/cruises/areas/brisbane1-331872.jpg',
            'https://images.dreamlines.de/neAg0h6EaoGV5s7jr2Pi4OMwOhs=/580x340/smart/filters:quality(50)/au/files/cruises/areas/sydney1-331873.jpg',
            'https://images.dreamlines.de/D4LXgvvQaJnkgyHcWuC2ykfuh5E=/580x340/smart/filters:quality(50)/au/files/cruises/areas/sydney7-331876.jpg',
            'https://images.dreamlines.de/hNOnNWw_4g7Hh88Sto2Nwb76-u8=/580x340/smart/filters:quality(50)/au/files/cruises/areas/tasmanien3-343349.jpg',
            'https://images.dreamlines.de/1d0z4i-vBbLAkQTs81XNtjyAGwc=/580x340/smart/filters:quality(50)/au/files/cruises/areas/byronbay-343350.jpg',

        ]
    });
    CruiseStore.sharedInstance.set({
        name: 'South Pacific',
        price: 2499,
        coverImageURL: 'https://images.dreamlines.de/CZmi-N37qJQqyGPrJIOdE-gYcQs=/580x340/smart/filters:quality(50)/au/files/cruises/areas/auckland1-331882.jpg',
        description: 'The South Pacific is iconic landscapes, a reflective beauty and an outback adventure ready to be discovered on a cruise. Sprinkled across the world\'s biggest ocean, the countries of the South Pacific are vast and diverse and include The Cook Islands, Fiji, Tonga, Kiribati, Tuvalu, Nauru, New Caledonia, Niue, Palau, Papua New Guinea, Easter Island and so much more.',
        imageURLs: [
            'https://images.dreamlines.de/CZmi-N37qJQqyGPrJIOdE-gYcQs=/580x340/smart/filters:quality(50)/au/files/cruises/areas/auckland1-331882.jpg',
            'https://images.dreamlines.de/CZmi-N37qJQqyGPrJIOdE-gYcQs=/580x340/smart/filters:quality(50)/au/files/cruises/areas/auckland1-331882.jpg',
            'https://images.dreamlines.de/S4Kux9lp0swm0-jEkBg-eaHGRJo=/580x340/smart/filters:quality(50)/au/files/cruises/areas/goldcoast1-331887.jpg',
            'https://images.dreamlines.de/mvLF0nzFFiPBArnRA_5pRgFkCuE=/580x340/smart/filters:quality(50)/au/files/cruises/areas/papuaneuguinea2-331890.jpg',
            'https://images.dreamlines.de/kEYMNO7DnuWFpxJu1g6VVCzJEec=/580x340/smart/filters:quality(50)/au/files/cruises/areas/akaroa4-331885.jpg'
        ]
    });
    CruiseStore.sharedInstance.set({
        name: 'Hawaii',
        price: 8099,
        coverImageURL: 'https://images.dreamlines.de/iBFoKRQ9YVaO_zw5XivZn5ZEEDg=/580x340/smart/filters:quality(50)/au/files/cruises/areas/mauibeach-349770.jpg',
        description: 'Hawaii has largely become synonymous with paradise. It is a multicultural gem where community traditions are kept alive by cultural festivals. It is made up of the descendants of ancient Polynesians, explorers of Europe, American missionaries and Asian plantation immigrants. Capture the sensational Hawaiian sunrises and sunsets and relax on luxurious golden-sand beaches.',
        imageURLs: [
            'https://images.dreamlines.de/iBFoKRQ9YVaO_zw5XivZn5ZEEDg=/580x340/smart/filters:quality(50)/au/files/cruises/areas/mauibeach-349770.jpg',
            'https://images.dreamlines.de/DbWACf98LMOXy1DUp6z7zE3mji8=/580x340/smart/filters:quality(50)/au/files/cruises/areas/honolulu2-331943.jpg',
            'https://images.dreamlines.de/zyF62u1BtJcP178t3jQQbIYvn7g=/580x340/smart/filters:quality(50)/au/files/cruises/areas/kauai-331945.jpg',
            'https://images.dreamlines.de/a_acUaKwDlW5BImNiOM8vzAxiTE=/580x340/smart/filters:quality(50)/au/files/cruises/areas/kauaihawaii-331946.jpg',
        ]
    });

    RequestStore.sharedInstance.set({
        userId: 2,
        cruiseId: 1
    });
})();
