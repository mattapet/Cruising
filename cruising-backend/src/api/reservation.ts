import * as express from 'express';
import { ReservationStore } from '../models/Reservation';
import { CruiseStore } from '../models/Cruise';
import { UserStore } from '../models/User';

export const ReservationRouter = express.Router();

ReservationRouter.route('')
.get((_: express.Request, res: express.Response) => {
    const reservations = ReservationStore.sharedInstance.getAll();
    res.status(200);
    res.send(
        CruiseStore.sharedInstance.populateReservations(
            UserStore.sharedInstance.populateReservations(
                reservations
    )));
});

ReservationRouter.route('/:reservation_id')
.put((req: express.Request, res: express.Response) =>  {
    const { reservation_id } = req.params;
    const { state } = req.body;
    const reservation = ReservationStore.sharedInstance.getById(parseInt(reservation_id));
    if (reservation) {
        reservation.state = state;
        ReservationStore.sharedInstance.set(reservation)
        res.status(200);
        res.send(
            CruiseStore.sharedInstance.populateReservations(
                UserStore.sharedInstance.populateReservations(
                    [reservation]
        ))[0]);
    } else {
        res.status(404);
        res.send({
            error: 'Not found',
            message: `Reservation with id \`${reservation_id}\` was not found.`
        });
    }
});
