import * as express from 'express';
import { RequestStore } from '../models/Request';
import { UserStore } from '../models/User';
import { CruiseStore } from '../models/Cruise';
import { ReservationState, ReservationStore } from '../models/Reservation';

export const RequestRouter = express.Router();

RequestRouter.route('')
.get((_: express.Request, res: express.Response) => {
    const requests = RequestStore.sharedInstance.getAll();
    res.status(200);
    res.send(
        CruiseStore.sharedInstance.populateRequests(
            UserStore.sharedInstance.populateRequests(
                requests
    )));
})
.post((req: express.Request, res: express.Response) => {
    const { userId, cruiseId } = req.body;
    const request = RequestStore.sharedInstance.set({
        userId: parseInt(userId),
        cruiseId: parseInt(cruiseId)
    });
    res.status(201);
    res.send(
        CruiseStore.sharedInstance.populateRequests(
            UserStore.sharedInstance.populateRequests(
                [request]
    ))[0]);
});


RequestRouter.route('/:request_id')
.post((req: express.Request, res: express.Response) => {
    const { request_id } = req.params;
    const confirmerId = req.body.confirmerId as number;
    const request = RequestStore.sharedInstance.getById(parseInt(request_id));
    if (request) {
        const reservation = ReservationStore.sharedInstance.set({
            state: ReservationState.initial,
            userId: request.userId,
            cruiseId: request.cruiseId,
            confirmerId: confirmerId
        });
        RequestStore.sharedInstance.removeById(parseInt(request_id));
        res.status(201);
        res.send(
            CruiseStore.sharedInstance.populateReservations(
                UserStore.sharedInstance.populateReservations(
                    [reservation]
        ))[0]);
    } else {
        res.status(404);
        res.send({
            error: 'Not found',
            message: `Request with id \`${request_id}\` was not found.`
        });
    }
})
.delete((req: express.Request, res: express.Response) => {
    const { request_id } = req.params;
    RequestStore.sharedInstance.removeById(parseInt(request_id));
    res.status(204);
    res.end();
});
