import * as express from 'express';
import { AccountStore } from '../models/Account';

export const LoginRouter = express.Router();

LoginRouter.post('/login', (req: express.Request, res: express.Response) => {
    const { email, password } = req.body;
    const account = AccountStore.sharedInstance.authenticate(email, password);
    if (account) {
        const user = account.user as any;
        res.status(200);
        res.send(user);

    } else {
        res.status(401);
        res.send({
            error: 'Unauthorized',
            message: 'Invalid credentials'
        });
    }
});

LoginRouter.post('/logout', (_: express.Request, res: express.Response) => {
    res.status(204);
    res.end();
});
