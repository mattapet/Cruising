import * as express from 'express';
import { UserStore } from '../models/User';
import { CruiseStore } from '../models/Cruise';
import { RequestStore } from '../models/Request';

export const UserRouter = express.Router();

UserRouter.route('')
.get((_: express.Request, res: express.Response) => {
    const users = UserStore.sharedInstance.getAll();
    res.status(200);
    res.send(users);
});

UserRouter.route('/:user_id/favorites')
.get((req: express.Request, res: express.Response) => {
    const { user_id } = req.params;
    const user = UserStore.sharedInstance.getById(parseInt(user_id));
    if (user) {
        const cruises = CruiseStore.sharedInstance.getByIds(user.favoriteIds);
        res.status(200);
        res.send(cruises);
    } else {
        res.status(404);
        res.send({
            error: 'Not found',
            message: `User with id \`${user_id}\` was not found.`
        });
    }
});

UserRouter.route('/:user_id/favorites/:cruise_id')
.post((req: express.Request, res: express.Response) => {
    const { user_id, cruise_id } = req.params;
    const user = UserStore.sharedInstance.getById(parseInt(user_id));
    if (user) {
        user.favoriteIds.push(parseInt(cruise_id));
        UserStore.sharedInstance.set(user);
        res.status(200);
        res.send(user);
    } else {
        res.status(404);
        res.send({
            error: 'Not found',
            message: `User with id \`${user_id}\` was not found.`
        });
    }
})
.delete((req: express.Request, res: express.Response) => {
    const { user_id, cruise_id } = req.params;
    const user = UserStore.sharedInstance.getById(parseInt(user_id));
    if (user) {
        const idx = user.favoriteIds.findIndex((value) => value === parseInt(cruise_id));
        if (idx >= 0) {
            user.favoriteIds.splice(idx, 1);
        }
        UserStore.sharedInstance.set(user);
        res.status(200);
        res.send(user);
    } else {
        res.status(404);
        res.send({
            error: 'Not found',
            message: `User with id \`${user_id}\` was not found.`
        });
    }
});

UserRouter.route('/:user_id/requests')
.get((req: express.Request, res: express.Response) => {
    const { user_id } = req.params;
    const requests = RequestStore.sharedInstance.getByUserId(parseInt(user_id))
    res.status(200);
    res.send(
        UserStore.sharedInstance.populateRequests(
            CruiseStore.sharedInstance.populateRequests(
                requests
    )));
});


