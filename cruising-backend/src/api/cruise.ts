import * as express from 'express';
import { CruiseStore } from '../models/Cruise';
import { UserStore } from '../models/User';
import { RequestStore } from '../models/Request';

export const CruiseRouter = express.Router();

CruiseRouter.get('', (_: express.Request, res: express.Response) => {
    const cruises = CruiseStore.sharedInstance.getAll();
    res.status(200);
    res.send(cruises);
});

CruiseRouter.route('/:cruise_id/requests')
.get((req: express.Request, res: express.Response) => {
    const { cruise_id } = req.params;
    const requests = RequestStore.sharedInstance.getByCruiseId(parseInt(cruise_id))
    res.status(200);
    res.send(
        UserStore.sharedInstance.populateRequests(
            CruiseStore.sharedInstance.populateRequests(
                requests
    )));
});
