//
//  State.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class State {
    var auth = AuthorizationState()
    var app = ApplicationState()
    
    init() {
        app.auth = auth
    }
}

class AuthorizationState {
    var user: User?
    var isLoggedIn: Bool {
        return user != nil
    }
    
    init() {
        let userDefaults = UserDefaults()
        guard let userData = userDefaults.data(forKey: "user")                      else { return }
        guard let user = try? Codables.apiDecoder.decode(User.self, from: userData) else { return }
        self.user = user
    }
    
    func store() {
        let userDefaults = UserDefaults()
        if let user = user {
            let userData = try? Codables.apiEncoder.encode(user)
            userDefaults.set(userData, forKey: "user")
        } else {
            userDefaults.set(nil, forKey: "user")
        }
    }
}

class ApplicationState {
    var auth: AuthorizationState!
    
    var users = UsersState()
    var cruises = CruisesState()
}

class CruisesState {
    var cruises: [Cruise] = [Cruise]()
    var selected: Cruise!
}

class UsersState {
    var users:   [User] = [User]()
    var selected: User!
}

