//
//  User+Cruise.swift
//  Cruising
//
//  Created by Peter Matta on 2/3/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol UserCruiseRelatable {
    var user: User { get }
    var cruise: Cruise { get }
}
