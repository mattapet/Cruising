//
//  Reservation.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class Reservation: Codable, UserCruiseRelatable {
    var id: Int
    var state: State
    var cruise: Cruise
    var user: User
    var confirmer: User
    
    enum State: String, Codable {
        case initial
        case confirmed
        case fulfilled
        case canceled
        case rejected
        
        var title: String {
            switch self {
            case .initial: return "Not confirmed"
            case .confirmed: return "Confirmed"
            case .fulfilled: return "Ended"
            case .canceled: return "Caneled"
            case .rejected: return "Rejected"
            }
        }
        
        var actionTitle: String? {
            switch self {
            case .confirmed: return "Confirm"
            case .canceled: return "Cancel"
            case .rejected: return "Reject"
            default: return nil
            }
        }
        
        var isDestructive: Bool {
            switch self {
            case .canceled, .rejected: return true
            default: return false
            }
        }
        
        var nextStates: [State]? {
            switch self {
            case .initial: return [.confirmed, .rejected]
            case .confirmed: return [.canceled]
            default: return nil
            }
        }
    }
}


// MARK: - Custom string convertible protocol conformance
extension Reservation: CustomStringConvertible {
    var description: String {
        return """
        [
            id: \(id),
            state: \(state),
            cruise: \(cruise),
            user: \(user),
            confirmer: \(confirmer)
        ]
        """
    }
}
