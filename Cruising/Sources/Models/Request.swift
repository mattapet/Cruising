//
//  Request.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class Request: Codable, UserCruiseRelatable {
    var id: Int
    var user: User
    var cruise: Cruise
}


// MARK: - Custom string convertible protocol conformance
extension Request: CustomStringConvertible {
    var description: String {
        return """
        [
            id: \(id),
            user: \(user),
            cruise: \(cruise)
        ]
        """
    }
}
