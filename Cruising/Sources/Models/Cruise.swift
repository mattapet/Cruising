//
//  Cruise.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class Cruise: Codable {
    var id: Int
    var price: Int
    var name: String
    var coverImageURL: URL
    var imageURLs: [URL]
    var cruiseDescription: String?

    // Cached property
    var requests: [Request]?
    var images: [Data]? = nil
    
    init(id: Int, name: String, price: Int, imageURL: URL, cruiseDescription: String?, imageURLs: [URL] = []) {
        self.id = id
        self.name = name
        self.price = price
        self.coverImageURL = imageURL
        self.cruiseDescription = cruiseDescription
        self.imageURLs = imageURLs
    }
}


// MARK: - Custom string convertible protocol conformance
extension Cruise: CustomStringConvertible {
    var description: String {
        return """
        [
            id: \(id),
            name: \(name),
            price: \(price)
            coverImageURL: \(coverImageURL),
            cruiseDescription: \(String(describing: cruiseDescription))
        ]
        """
    }
}

private extension Cruise {
    enum CodingKeys: String, CodingKey {
        case id
        case price
        case name
        case coverImageURL
        case cruiseDescription = "description"
        case requests
        case imageURLs
    }
}
