//
//  User.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class User: Codable {
    var id: Int
    var firstName: String
    var lastName: String
    var email: String
    var birthDate: Date
    var gender: Gender

    var favoriteIds: Set<Int>

    init(id: Int, firstName: String, lastName: String, email: String, birthDate: Date, gender: Gender, favorites: Set<Int>) {
        self.id = id
        self.firstName = firstName
        self.lastName  = lastName
        self.email     = email
        self.birthDate = birthDate
        self.gender    = gender
        self.favoriteIds = favorites
    }
    
    enum Gender: String, Codable {
        case male
        case female
    }
}

// MARK: - Convenience computed properties
extension User {
    var fullName: String {
        return "\(firstName) \(lastName)"
    }
    
    var age: Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: birthDate, to: Date())
        return components.year ?? 0
    }
}

// MARK: - Custom string convertible protocol conformance
extension User: CustomStringConvertible {
    var description: String {
        return """
        [
            id: \(id),
            firstName: \(firstName),
            lastName: \(lastName),
            email: \(email),
            birthDate: \(birthDate),
            gender: \(gender)
            favoriteIds: \(favoriteIds)
        ]
        """
    }
}

