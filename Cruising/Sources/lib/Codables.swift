//
//  Codables.swift
//  Cruising
//
//  Created by Peter Matta on 1/19/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

enum DateError: String, Error {
    case invalidDate
}

enum Codables {
    static let apiEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(Formatters.apiDateFormatter)
        return encoder
    }()
    
    static let apiDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(Formatters.apiDateFormatter)
        return decoder
    }()
}
