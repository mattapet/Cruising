//
//  SlideshowCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class SlideshowCell: UITableViewCell {
    weak var spinner: UIActivityIndicatorView!
    var slideShow: ImageSlideShow!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        slideShow = ImageSlideShow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimating() {
        if self.spinner != nil { return }
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        contentView.addSubview(spinner)
        spinner.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        spinner.startAnimating()
        self.spinner = spinner
    }
    
    func stopAnimating() {
        spinner?.removeFromSuperview()
        contentView.addSubview(slideShow.view)
        slideShow.view.snp.removeConstraints()
        slideShow.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func update(_ images: [Data]?) {
        if let images = images {
            let imageViews = images.map { UIImageView(image: UIImage(data: $0)) }
            stopAnimating()
            slideShow.setItems(imageViews)
        } else {
            startAnimating()
        }
    }
    
    class var reuseIdentifier: String {
        return "SlideshowCell"
    }
}
