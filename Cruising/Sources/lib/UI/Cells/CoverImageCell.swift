//
//  CoverImageCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class CoverImageCell: UITableViewCell {
    weak var coverImage: UIImageView!
    
    // MARK: - Initilization
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        let coverImage = UIImageView()
        contentView.addSubview(coverImage)
        coverImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.coverImage = coverImage
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
    // MARK: - Content update
    
    func update(_ image: UIImageView) {
        coverImage.removeFromSuperview()
        contentView.addSubview(image)
        image.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.coverImage = image
    }
    
    func update(_ url: URL) {
        if let coverImage = coverImage as? URLImageView {
            coverImage.url = url
        } else {
            let coverImage = URLImageView(url: url)
            update(coverImage)
        }
    }
    
    
    // MARK: - Class properties
    
    class var reuseIdentifier: String {
        return "CoverImageCell"
    }
    
}
