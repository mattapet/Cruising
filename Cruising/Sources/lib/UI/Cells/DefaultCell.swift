//
//  DefaultCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class DefaultCell: UITableViewCell {
    weak var titleLabel: UILabel!
    
    
    // MARK: - Initialization
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let titleLabel = UILabel()
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        self.titleLabel = titleLabel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Class properties
    class var reuseIdentifier: String {
        return "DefaultCell"
    }
}
