//
//  ProfileNameCell.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class TitleCell: UITableViewCell {
    weak var titleLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        let titleLabel = UILabel()
        contentView.addSubview(titleLabel)
        titleLabel.textAlignment = .center
        titleLabel.font.withSize(24.0)
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.titleLabel = titleLabel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class var reuseIdentifier: String {
        return "TitleCell"
    }
}
