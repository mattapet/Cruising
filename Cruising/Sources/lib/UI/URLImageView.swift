//
//  URLImageView.swift
//  Cruising
//
//  Created by Peter Matta on 12/25/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class URLImageView: UIImageView {
    /// Content view into which to display an activity indicator
    weak var contentView: UIView!
    weak var spinner: UIActivityIndicatorView!
    weak var retryImage: UIImageView!
    weak var tapGestureRecognizer: UITapGestureRecognizer!
    /// Source of the image to display.
    var url: URL? { didSet {
        if url != oldValue {
            fetch()
        }
    }}
    
    
    // MARK: - Initializers
    
    init() {
        super.init(image: nil)
    }
    
    init(url: URL) {
        super.init(image: nil)
        self.url = url
        fetch()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Internal view content handlers
    
    internal func display(_ image: UIImage) {
        contentView?.removeFromSuperview()
        self.image = image
    }
    
    internal func displayActivity() {
        image = nil
        let contentView = UIView()
        addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.contentView = contentView
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        contentView.addSubview(spinner)
        spinner.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        spinner.startAnimating()
        self.spinner = spinner
    }
    
    internal func displayRetry() {
        spinner.removeFromSuperview()
        let retryImage = UIImageView(image: UIImage(named: "retry_icon"))
        contentView.addSubview(retryImage)
        retryImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.retryImage = retryImage
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(retry))
        contentView.addGestureRecognizer(tapGestureRecognizer)
        self.tapGestureRecognizer = tapGestureRecognizer
        isUserInteractionEnabled = true
    }
    
    @objc private func retry() {
        isUserInteractionEnabled = false
        contentView.removeFromSuperview()
        fetch()
    }
    
}


// MARK: - Fetching handler
extension URLImageView {
    private func fetch() {
        if let url = url {
            displayActivity()
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                do {
                    let data = try Data(contentsOf: url)
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async { [weak self] in
                            self?.display(image)
                        }
                        return
                    }
                } catch let err {
                    print(err)
                }
                DispatchQueue.main.async { [weak self] in
                    self?.displayRetry()
                }
            }
        }
    }
}

