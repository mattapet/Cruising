//
//  GenericTableViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/3/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class GenericTableViewCell<T>: UITableViewCell {
    var item: T! { didSet {
        update()
    }}
    
    internal func update() { }
    
    class var reuseIdentifier: String {
        return "GenericTableViewCell"
    }
}


/// Generic dynamic table view controller.
class GenericTableViewController<T, U: GenericTableViewCell<T>>: UITableViewController {
    var items: [T] {
        return []
    }
    
    var displayStyle: DisplayStyle = .rows { didSet {
        tableView.reloadData()
    }}
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        setupRefreshControl()
        setupReusableCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.refreshControl?.endRefreshing()
    }
    
    
    // MARK: - Internal methods
    
    internal func setupRefreshControl() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(
            self,
            action: #selector(onRefresh),
               for: .valueChanged
        )
    }
    
    internal func setupReusableCells() {
        tableView.register(
            U.self,
            forCellReuseIdentifier: U.reuseIdentifier
        )
    }
    
    internal func itemFor(_ indexPath: IndexPath) -> T {
        let idx = displayStyle == .rows ? indexPath.row : indexPath.section
        return items[idx]
    }
    
    @objc internal func onRefresh() { }
    
    
    // MARK: - Table view data source protocol conformance
    
    // - Data
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayStyle == .rows ? 1 : items.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayStyle == .rows ? items.count : 1
    }
    
    // - Views
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: U.reuseIdentifier,
                       for: indexPath
        ) as! U
        cell.item = itemFor(indexPath)
        return cell
    }
    
    
    // MARK: - Table view delegate protocol conformance
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


// MARK: - Display style type
extension GenericTableViewController {
    enum DisplayStyle {
        case sections
        case rows
    }
}

