//
//  BudgetPicker.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

@objc protocol BudgetPickerDelegate: NSObjectProtocol {
    func budgetPickerDidChange(_ picker: BudgetPicker)
}

class BudgetPicker: PickerView {
    // MARK: - Properties
    private let components: [Component] = [.min, .max]
    
    var minSelected: Row = .header
    var maxSelected: Row = .header
    
    weak var delegate: BudgetPickerDelegate?
    
    
    // MARK: - Initializations
    
    override init() {
        super.init()
        pickerView.dataSource = self
        pickerView.delegate = self
        updateTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - Utility methods
fileprivate extension BudgetPicker {
    func updateTextField() {
        textField.text = "\(titleFor(minSelected, forComponent: .min)) - \(titleFor(maxSelected, forComponent: .max))"
        delegate?.budgetPickerDidChange(self)
    }
    
    func titleFor(_ row: Row, forComponent component: Component) -> String {
        switch row {
        case .header: return component.header
        case .any: return "Any"
        default: return Formatters.currencyFormatter.string(from: NSNumber(integerLiteral: row.rawValue))!
        }
    }
}


// MARK: - Toolbar handlers
internal extension BudgetPicker {
    @objc override func onDoneButtonTapped() {
        super.onDoneButtonTapped()
        updateTextField()
    }
}


// MARK: - Picker view data source protocol conformance
extension BudgetPicker: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return components.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return components[component].rows.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titleFor(components[component].rows[row], forComponent: components[component])
    }
}


// MARK: - Picker view delegate protocol conformance
extension BudgetPicker: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch components[component] {
        case .min: minSelected = Component.min.rows[row]
        case .max: maxSelected = Component.max.rows[row]
        }
    }
}



// MARK: - Picker components and rows
extension BudgetPicker {
    enum Row: Int {
        case header = 0
        case any = 1
        case lowest = 5000
        case low = 7500
        case midlow = 10000
        case mid = 15000
        case midhigh = 20000
        case high = 30000
        case highest = 50000
    }
    
    enum Component {
        case min
        case max
        
        var rows: [Row] {
            switch self {
            case .min: return [.header, .lowest, .low, .midlow, .mid, .any]
            case .max: return [.header, .mid, .midhigh, .high, .highest, .any]
            }
        }
        
        var header: String {
            switch self {
            case .min: return "Min"
            case .max: return "Max"
            }
        }
    }
}


