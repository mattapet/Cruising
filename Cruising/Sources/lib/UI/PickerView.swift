//
//  RangePicker.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class PickerView: UIView {
    // MARK: - UI Properties
    
    weak var textField: UITextField!
    weak var pickerView: UIPickerView!
    weak var toolbar: UIToolbar!

    
    // MARK: - Initialization
    
    init() {
        super.init(frame: CGRect())
        let textField = UITextField()
        addSubview(textField)
        textField.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        textField.tintColor = .clear
        self.textField = textField
        
        let pickerView = UIPickerView()
        pickerView.showsSelectionIndicator = true
        self.pickerView = pickerView
        textField.inputView = pickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .`default`
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        toolbar.tintColor = .`default`
        toolbar.isUserInteractionEnabled = true
        
        let cancelButton = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(onCancelButtonTapped)
        )
        let middleFill = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil
        )
        let doneButton = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(onDoneButtonTapped)
        )
        toolbar.setItems([cancelButton, middleFill, doneButton], animated: false)
        self.toolbar = toolbar
        textField.inputAccessoryView = toolbar
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - Toolbar handlers
internal extension PickerView {
    @objc func onCancelButtonTapped() {
        textField.resignFirstResponder()
    }
    
    @objc func onDoneButtonTapped() {
        textField.resignFirstResponder()
    }
}


