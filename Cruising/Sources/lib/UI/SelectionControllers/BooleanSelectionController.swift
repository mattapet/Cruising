//
//  BooleanSelectionController.swift
//  Cruising
//
//  Created by Peter Matta on 1/5/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

class BooleanSelectionController: ListSelectionViewController {
    override var list: [(label: String, value: Any?)] {
        return [
            ("Yes", true),
            ("No", false)
        ]
    }
}
