//
//  GenderSelectionController.swift
//  Cruising
//
//  Created by Peter Matta on 1/5/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class GenderSelectionController: ListSelectionViewController {
    override var isAnyEnabled: Bool { return true }
    override var list: [(label: String, value: Any?)] {
        return [
            ("male", User.Gender.male),
            ("female", User.Gender.female)
        ]
    }
}
