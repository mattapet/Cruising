//
//  ImageSlideShow.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class ImageSlideShow: UIViewController {
    weak var scrollView: UIScrollView!
    weak var contentView: UIStackView!
    
    var items: [UIView] {
        return contentView?.arrangedSubviews ?? []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        scrollView.alwaysBounceHorizontal = true
        scrollView.isUserInteractionEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        self.scrollView = scrollView
        
        let contentView = UIStackView()
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.bottom.height.width.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        contentView.axis = .horizontal
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView = contentView
    }
    
    func setItems(_ items: [UIView]) {
        self.contentView.removeFromSuperview()
        
        let contentView = UIStackView(arrangedSubviews: items)
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.bottom.height.leading.trailing.equalToSuperview()
        }
        contentView.axis = .horizontal
        contentView.alignment = .fill
        contentView.distribution = .fillEqually
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView = contentView
        
        items.forEach { item in
            item.snp.removeConstraints()
            item.snp.makeConstraints { make in
                make.width.height.equalTo(scrollView)
            }
        }
    }
}
