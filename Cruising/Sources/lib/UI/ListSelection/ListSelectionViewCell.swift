//
//  ListSelectionViewCell.swift
//  Cruising
//
//  Created by Peter Matta on 1/5/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class ListSelectionViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
}
