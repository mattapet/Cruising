//
//  ListSelectionViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/5/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

protocol ListSelectionViewDelegate {
    func listSelect(_ listSelect: ListSelectionViewController, didSelectValue value: Any?)
}

class ListSelectionViewController: UITableViewController {
    // MARK: - Public stored properties
    var delegate: ListSelectionViewDelegate?
    
    // MARK: - Internal computed properties
    internal var list: [(label: String, value: Any?)] {
        return [(label: String, value: Any?)]()
    }
    internal var isAnyEnabled: Bool {
        return false
    }
    
    // MARK: - Private computed properties
    private var _list: [(label: String, value: Any?)] {
        var list = self.list
        if isAnyEnabled {
            list.append(("Any", nil))
        }
        return list
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
}

// MARK: - Table view data source
extension ListSelectionViewController {
    final override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    final override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _list.count
    }
    
    final override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "ListSelectionViewCell",
            for: indexPath
        ) as! ListSelectionViewCell
        cell.label.text = _list[indexPath.row].label
        return cell
    }
}


// MARK: - Table view delegate
extension ListSelectionViewController {
    final override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selected = _list[indexPath.row]
        delegate?.listSelect(self, didSelectValue: selected.value)
        navigationController?.popViewController(animated: true)
    }
}
