//
//  ProfilePictureView.swift
//  Cruising
//
//  Created by Peter Matta on 12/30/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit

class ProfilePictureView: UIImageView {
    var gender: User.Gender { didSet {
        image = UIImage(named: iconName)
    }}
    var iconName: String {
        return "profile_icon_\(gender == .male ? "" : "female_")outlined"
    }
    
    convenience init(user: User) {
        self.init(gender: user.gender)
    }
    
    required init(gender: User.Gender = .male) {
        self.gender = gender
        super.init(image: UIImage(named: iconName))
        backgroundColor = .groupTableViewBackground
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func round() {
        layer.cornerRadius = bounds.size.width / 2
        clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        round()
    }
}
