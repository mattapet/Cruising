//
//  Formatters.swift
//  Cruising
//
//  Created by Peter Matta on 1/6/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

enum Formatters {
    static let numberFormatter: NumberFormatter = {
       let formatter = NumberFormatter()
        formatter.allowsFloats = false
        formatter.groupingSeparator = ","
        return formatter
    }()
    
    static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.allowsFloats = false
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    
    static let mediumDateFormatter: DateFormatter = {
       let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.setLocalizedDateFormatFromTemplate("MMMd Y")
        return formatter
    }()
    
    static let fullDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.setLocalizedDateFormatFromTemplate("MMMMd Y")
        return formatter
    }()
    
    static let apiDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
