//
//  ReservationManager.swift
//  Cruising
//
//  Created by Peter Matta on 2/9/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol ReservationManagerDelegate: class {
    func reservationManager(_ manager: ReservationManager, didUpdateStateForReservation reservation: Reservation)
    func reservationManager(_ manager: ReservationManager, didFailUpdateStateWithError error: Error)
}

class ReservationManager {
    weak var delegate: ReservationManagerDelegate?
    
    func updateState(_ state: Reservation.State, for reservation: Reservation) {
        API.ReservationAPI.updateState(state, for: reservation) { [unowned self] (reservation, error) in
            if let error = error {
                self.delegate?.reservationManager(self, didFailUpdateStateWithError: error)
            } else {
                self.delegate?.reservationManager(self, didUpdateStateForReservation: reservation!)
            }
        }
    }
}

