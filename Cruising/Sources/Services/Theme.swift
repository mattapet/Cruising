//
//  Theme.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

extension UIColor {
    class var `default`: UIColor {
        return black
    }
}

enum Theme {
    static func button(title: String, color: UIColor = .`default`) -> UIButton {
        let button = UIButton()
        button.titleLabel?.textAlignment = .center
        button.setTitle(title, for: .normal)
        button.setTitleColor(color, for: .normal)
        button.setTitleColor(.gray, for: .focused)
        button.setTitleColor(.gray, for: .highlighted)
        button.setTitleColor(.gray, for: .disabled)
        return button
    }
}

