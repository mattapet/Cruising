//
//  RequestManager.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol RequestFetcherDelegate: class {
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllRequests requests: [Request])
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllRequestsWithError error: Error)
    
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllForCruise requests: [Request])
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllForCruiseWithError error: Error)
    
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllForUser requests: [Request])
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllForUserWithError error: Error)
    
    func requestFetcher(_ fetcher: RequestFetcher, didCreateRequest request: Request)
    func requestFetcher(_ fetcher: RequestFetcher, didFailCreateRequestWithError error: Error)
}

class RequestFetcher {
    weak var delegate: RequestFetcherDelegate?
}


// MARK: - Fetching methods
extension RequestFetcher {
    func fetchAll() {
        API.RequestAPI.fetchAll { [unowned self] (requests, error) in
            if let error = error {
                self.delegate?.requestFetcher(self, didFailFetchAllRequestsWithError: error)
            } else {
                self.delegate?.requestFetcher(self, didFetchAllRequests: requests!)
            }
        }
    }
    
    func fetchAll(`for` cruise: Cruise) {
        API.RequestAPI.fetchAll(for: cruise) { [unowned self] (requests, error) in
            if let error = error {
                self.delegate?.requestFetcher(self, didFailFetchAllForCruiseWithError: error)
            } else {
                self.delegate?.requestFetcher(self, didFetchAllForCruise: requests!)
            }
        }
    }
    
    func fetchAll(`for` user: User) {
        API.RequestAPI.fetchAll(for: user) { [unowned self] (requests, error) in
            if let error = error {
                self.delegate?.requestFetcher(self, didFailFetchAllForUserWithError: error)
            } else {
                self.delegate?.requestFetcher(self, didFetchAllForUser: requests!)
            }
        }
    }
}


// MAKR: - Creating methods
extension RequestFetcher {
    func createRequest(`for` cruise: Cruise, with user: User) {
        API.RequestAPI.create(for: cruise, with: user) { [unowned self] (request, error) in
            if let error = error {
                self.delegate?.requestFetcher(self, didFailCreateRequestWithError: error)
            } else {
                self.delegate?.requestFetcher(self, didCreateRequest: request!)
            }
        }
    }
}
