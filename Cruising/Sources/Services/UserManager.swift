//
//  UserManager.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol UserManagerDelegate: class {
    func userManager(_ manager: UserManager, didUpdateUsers users: [User])
    
    func userManager(_ manager: UserManager, didFetchAllUsers users: [User])
    func userManager(_ manager: UserManager, didFailFetchAllUsersWithError error: Error)
}

class UserManager {
    weak var delegate: UserManagerDelegate?
}


// MARK: - Fetching methods
extension UserManager {
    func fetchAll() {
        API.UserAPI.fetchAll { [unowned self] (users, error) in
            if let error = error {
                self.delegate?.userManager(self, didFailFetchAllUsersWithError: error)
            } else {
                self.delegate?.userManager(self, didFetchAllUsers: users!)
            }
        }
    }
}
