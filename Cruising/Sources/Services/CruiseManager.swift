//
//  CruiseManager.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol CruiseManagerDelegate: class {
    func cruiseManager(_ manager: CruiseManager, didFetchAllCruises cruises: [Cruise])
    func cruiseManager(_ manager: CruiseManager, didFailFetchAllCrusesWithError error: Error)
    
    func cruiseManager(_ manager: CruiseManager, didFetchAllFavorites favorites: [Cruise])
    func cruiseManager(_ manager: CruiseManager, didFailFetchAllFavoritesWithError error: Error)
}

class CruiseManager {
    weak var delegate: CruiseManagerDelegate?
    
    func fetchAll() {
        API.CruiseAPI.fetchAll { [unowned self] (cruises, error) in
            if let error = error {
                self.delegate?.cruiseManager(self, didFailFetchAllCrusesWithError: error)
            } else {
                self.delegate?.cruiseManager(self, didFetchAllCruises: cruises!)
            }
        }
    }
    
    func fetchFavorites(`for` user: User) {
        API.CruiseAPI.fetchFavorites(for: user) { [unowned self] (cruises, error) in
            if let error = error {
                self.delegate?.cruiseManager(self, didFailFetchAllFavoritesWithError: error)
            } else {
                self.delegate?.cruiseManager(self, didFetchAllFavorites: cruises!)
            }
        }
    }
}
