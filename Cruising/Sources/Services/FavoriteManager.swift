//
//  FavoriteManager.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol FavoriteManagerDelegate: class {
    func favoriteManager(_ manager: FavoriteManager, didAddFavoriteCruise cruise: Cruise)
    func favoriteManager(_ manager: FavoriteManager, didFailAddFavoriteCruiseWithError error: Error)
    
    func favoriteManager(_ manager: FavoriteManager, didRemoveFavoriteCruise cruise: Cruise)
    func favoriteManager(_ manager: FavoriteManager, didFailRemoveFavoriteCruiseWithError error: Error)
}

class FavoriteManager {
    weak var delegate: FavoriteManagerDelegate?
    
    func addFavorite(_ cruise: Cruise, to user: User) {
        API.CruiseAPI.addFavorite(cruise, to: user) { [unowned self] (user, error) in
            if let error = error {
                self.delegate?.favoriteManager(self, didFailAddFavoriteCruiseWithError: error)
            } else {
                self.delegate?.favoriteManager(self, didAddFavoriteCruise: cruise)
            }
        }
    }
    
    func removeFavorite(_ cruise: Cruise, from user: User) {
        API.CruiseAPI.removeFavorite(cruise, from: user) { [unowned self] (user, error) in
            if let error = error {
                self.delegate?.favoriteManager(self, didFailRemoveFavoriteCruiseWithError: error)
            } else {
                self.delegate?.favoriteManager(self, didRemoveFavoriteCruise: cruise)
            }
        }
    }
}
