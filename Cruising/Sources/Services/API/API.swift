//
//  API.swift
//  Cruising
//
//  Created by Peter Matta on 12/25/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

enum API {
    internal static let BASE_URL: String      = "http://localhost:3000"
    internal static let decoder : JSONDecoder =  Codables.apiDecoder
    
    internal static func completion<T: Codable>(_ closure: @escaping (T?, Error?) -> Void) -> ((DataResponse<Any>) -> Void) {
        return { response in
            if let data = response.data {
                do {
                    let parsed = try Codables.apiDecoder.decode(T.self, from: data)
                    closure(parsed, nil)
                } catch let error {
                    print(error)
                    closure(nil, error)
                }
                return
            }
            closure(nil, response.error)
        }
    }
}
