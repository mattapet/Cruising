//
//  RequestAPI.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    enum RequestAPI {
        static func fetchAll(withCompletion completion: @escaping ([Request]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/request"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func fetchAll(`for` cruise: Cruise, withCompletion completion: @escaping ([Request]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/cruise/\(cruise.id)/requests"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func fetchAll(`for` user: User, withCompletion completion: @escaping ([Request]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/user/\(user.id)/requests"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func create(`for` cruise: Cruise, with user: User, withCompletion completion: @escaping (Request?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/request"
            let params: [String:Any] = [
                "cruiseId": cruise.id,
                "userId": user.id
            ]
            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.`default`)
            .responseJSON(completionHandler: API.completion(completion))
        }
        
        static func accept(_ request: Request, with user: User, withCompletion completion: @escaping (Reservation?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/request/\(request.id)"
            let params: [String:Any] = [
                "confirmerId": user.id
            ]
            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.`default`)
            .responseJSON(completionHandler: API.completion(completion))
        }
        
        static func remove(_ request: Request, withCompletion completion: @escaping (Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/request/\(request.id)"
            Alamofire.request(url, method: .delete).responseJSON { completion($0.error) }
        }
    }
}

