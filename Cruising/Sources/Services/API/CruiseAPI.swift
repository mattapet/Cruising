//
//  CruiseAPI.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    enum CruiseAPI {
        static func fetchAll(withCompletion completion: @escaping ([Cruise]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/cruise"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func fetchFavorites(`for` user: User, withCompletion completion: @escaping ([Cruise]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/user/\(user.id)/favorites"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func addFavorite(_ cruise: Cruise, to user: User, withCompletion completion: @escaping (User?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/user/\(user.id)/favorites/\(cruise.id)"
            Alamofire.request(url, method: .post).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func removeFavorite(_ cruise: Cruise, from user: User, withCompletion completion: @escaping (User?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/user/\(user.id)/favorites/\(cruise.id)"
            Alamofire.request(url, method: .delete).responseJSON(completionHandler: API.completion(completion))
        }
    }
}
