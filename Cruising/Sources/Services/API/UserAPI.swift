//
//  UserAPI.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    enum UserAPI {
        static func fetchAll(withCompletion completion: @escaping ([User]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/user"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
    }
}
