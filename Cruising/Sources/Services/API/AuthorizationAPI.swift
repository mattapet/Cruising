//
//  AuthorizationAPI.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    enum AuthorizationAPI {
        static func login(email: String, password: String, withCompletion completion: @escaping (User?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/login"
            let params: [String:Any] = [
                "email": email,
                "password": password
            ]

            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.`default`)
            .responseJSON(completionHandler: API.completion(completion))
        }
        
        static func logout(withCompletion completion: @escaping (Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/logout"
            Alamofire.request(url, method: .post).response { completion($0.error) }
        }
    }
}
