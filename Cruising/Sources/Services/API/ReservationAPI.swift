//
//  ReservationAPI.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    enum ReservationAPI {
        static func fetchAll(withCompletion completion: @escaping ([Reservation]?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/reservation"
            Alamofire.request(url).responseJSON(completionHandler: API.completion(completion))
        }
        
        static func updateState(_ state: Reservation.State, for reservation: Reservation, withCompletion completion: @escaping (Reservation?, Error?) -> Void) {
            let url = "\(API.BASE_URL)/api/reservation/\(reservation.id)"
            let params = [
                "state": state.rawValue
            ]
            Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.`default`)
            .responseJSON(completionHandler: API.completion(completion))
        }
    }
}

