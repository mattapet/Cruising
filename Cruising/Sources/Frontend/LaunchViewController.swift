//
//  LaunchViewController.swift
//  Cruising
//
//  Created by Peter Matta on 12/23/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class LaunchViewController: UIViewController {
    weak var state: State!
    weak var activeVC: UIViewController!
    weak var spinner: UIActivityIndicatorView!
    
    // MARK: - View controller lifecycle
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        view.addSubview(spinner)
        spinner.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        spinner.startAnimating()
        self.spinner = spinner
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if state.auth.isLoggedIn {
            presentApplicationViewController()
        } else {
            presentLoginViewController()
        }
    }
}


// MARK: - Navigation
fileprivate extension LaunchViewController {
    func presentApplicationViewController() {
        let appVC = ApplicationViewController(appState: state.app)
        appVC.modalTransitionStyle = .flipHorizontal
        present(appVC, animated: true)
    }
    
    func presentLoginViewController() {
        let loginVC = LoginViewController()
        loginVC.authState = state.auth
        present(loginVC, animated: true)
    }
}
