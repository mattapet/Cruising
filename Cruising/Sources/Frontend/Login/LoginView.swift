//
//  LoginView.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

internal extension LoginViewController {
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        
        let emailTextField = UITextField()
        self.emailTextField = emailTextField
        setupEmailTextField()
        
        let passwordTextField = UITextField()
        self.passwordTextField = passwordTextField
        setupPasswordTextField()
        
        let loginButton = Theme.button(title: "Login")
        self.loginButton = loginButton
        
        
        let container = UIStackView(arrangedSubviews: [
            emailTextField,
            passwordTextField,
            loginButton
        ])
        view.addSubview(container)
        self.container = container
        setupContainer()
        layoutContainer()
        
        let logoImage = UIImageView(image: UIImage(named: "app_icon_image"))
        view.addSubview(logoImage)
        logoImage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(container.snp.top).offset(-10)
            make.height.width.equalTo(64)
        }
        
        layourEmailTextField()
        layoutPasswordTextField()
        layoutLoginButton()
    }
}


// MARK: - Container view setup
 fileprivate extension LoginViewController {
    func setupContainer() {
        container.axis = .vertical
        container.spacing = 10
        container.alignment = .center
        container.distribution = .fill
        container.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func layoutContainer() {
        container.snp.makeConstraints({ make in
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
            make.centerY.equalToSuperview()
        })
    }
}


// MARK: - Email text field setup
fileprivate extension LoginViewController {
    func setupEmailTextField() {
        emailTextField.placeholder = "Email"
        emailTextField.keyboardType = .emailAddress
        emailTextField.returnKeyType = .next
        emailTextField.autocorrectionType = .no
        emailTextField.autocapitalizationType = .none
        emailTextField.textContentType = .emailAddress
        emailTextField.enablesReturnKeyAutomatically = true
        emailTextField.borderStyle = .roundedRect
    }
    
    func layourEmailTextField() {
        emailTextField.snp.makeConstraints { make in
            make.width.equalToSuperview()
        }
    }
}


// MARK: - Password text field setup {
fileprivate extension LoginViewController {
    func setupPasswordTextField() {
        passwordTextField.placeholder = "Password"
        passwordTextField.keyboardType = .asciiCapable
        passwordTextField.returnKeyType = .`default`
        passwordTextField.isSecureTextEntry = true
        passwordTextField.textContentType = .password
        passwordTextField.enablesReturnKeyAutomatically = true
        passwordTextField.clearsOnBeginEditing = true
        passwordTextField.borderStyle = .roundedRect
        
    }
    
    func layoutPasswordTextField() {
        passwordTextField.snp.makeConstraints { make in
            make.width.equalToSuperview()
        }
    }
}



// MARK: - Login button setup
fileprivate extension LoginViewController {
    func layoutLoginButton() {
        loginButton.snp.makeConstraints { make in
            make.width.equalToSuperview()
        }
    }
}
