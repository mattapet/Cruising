//
//  LoginManager.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol LoginManagerDelegate: class {
    func loginManager(_ manager: LoginManager, didLoginUser user: User)
    func loginManager(_ manager: LoginManager, didFailLoginUserWithError error: Error)
}

class LoginManager {
    weak var delegate: LoginManagerDelegate?
    
    func login(email: String, password: String) {
        API.AuthorizationAPI.login(email: email, password: password) { [unowned self] (user, error) in
            if let error = error {
                self.delegate?.loginManager(self, didFailLoginUserWithError: error)
            } else {
                self.delegate?.loginManager(self, didLoginUser: user!)
            }
        }
    }
}
