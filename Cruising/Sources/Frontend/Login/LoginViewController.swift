//
//  LoginViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    // Dependency injection
    var authState: AuthorizationState!
    
    // MARK: - Private proepries
    private let loginManager = LoginManager()
    
    private var canLogin: Bool {
        return emailTextField.hasText && passwordTextField.hasText
    }
    
    // MARK: - View references
    weak var container        : UIStackView!
    weak var emailTextField   : UITextField!
    weak var passwordTextField: UITextField!
    weak var loginButton      : UIButton!
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginManager.delegate = self
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.addTarget(self, action: #selector(onTextFieldChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(onTextFieldChange(_:)), for: .editingChanged)
        loginButton.addTarget(self, action: #selector(onLoginButtonTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loginButton.isEnabled = canLogin
    }
    
    
    // MARK: - Private methode
    private func login() {
        setContentEnabled(false)
        let email    = emailTextField.text!
        let password = passwordTextField.text!
        loginManager.login(email: email, password: password)
    }
}


// MARK: - UI handlers
fileprivate extension LoginViewController {
    @objc func onLoginButtonTapped() {
        login()
    }
    
    @objc func onTextFieldChange(_ textField: UITextField) {
        loginButton.isEnabled = canLogin
    }
    
    func setContentEnabled(_ enabled: Bool) {
        emailTextField.isEnabled = enabled
        passwordTextField.isEnabled = enabled
        loginButton.isEnabled = enabled
    }
}


// MARK: - Text field delegate protocol conformance
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            view.endEditing(true)
            login()
        }
        return true
    }
}


// MARK: - Login manager delegate protocol conformance
extension LoginViewController: LoginManagerDelegate {
    func loginManager(_ manager: LoginManager, didLoginUser user: User) {
        authState.user = user
        authState.store()
        dismiss(animated: true)
    }
    
    func loginManager(_ manager: LoginManager, didFailLoginUserWithError error: Error) {
        setContentEnabled(true)
        passwordTextField.becomeFirstResponder()
    }
}
