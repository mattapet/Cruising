//
//  RequestsViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class RequestsViewController: UserCruiseRelatableListViewController {
    private let requestFetcher = RequestFetcher()
    
    var user: User!
    var requests = [Request]()
    
    fileprivate let filterOptions: [Filter] = [.favorite, .users, .none]
    fileprivate var filter: Filter = .none { didSet {
        resolveFilterIcon()
    }}
    
    override var items: [UserCruiseRelatable] {
        switch filter {
        case .favorite: return requests.filter { user.favoriteIds.contains($0.cruise.id) }
        case .users: return requests.filter { $0.user.id == user.id }
        case .none: return requests
        }
    }
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestFetcher.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resolveFilterIcon()
        requestFetcher.fetchAll()
        tableView.refreshControl?.beginRefreshing()
    }
    
    
    // MARK: UI handlers
    
    override internal func onRefresh() {
        requestFetcher.fetchAll()
    }
    
    @objc internal func onFilterTapped() {
        resolveFilterIcon()
        let alert = UIAlertController(
                     title: "Filter Requests",
                   message: nil,
            preferredStyle: .actionSheet
        )
        filterOptions.forEach { option in
            let action = UIAlertAction(
                title: option.title,
                style: .`default`) { [unowned self] _ in self.filter = option }
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }
    
    private func resolveFilterIcon() {
        let iconName = "filter_icon\(filter != .none ? "" : "_outlined")"
        let image = UIImage(named: iconName)
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: image,
            style: .plain,
            target: self,
            action: #selector(onFilterTapped)
        )
        tableView.reloadData()
    }
    
    
    // MARK: - Table view delegate protocol conformance
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = itemFor(indexPath)
        let detailVC = RequestDetailViewController()
        detailVC.user = user
        detailVC.request = item as! Request
        detailVC.requests = requests
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension RequestsViewController: RequestFetcherDelegate {
    func requestFetcher(_ manager: RequestFetcher, didFetchAllRequests requests: [Request]) {
        self.requests = requests
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    func requestFetcher(_ manager: RequestFetcher, didFailFetchAllRequestsWithError error: Error) {
        tableView.refreshControl?.endRefreshing()
    }
    
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllForCruise requests: [Request]) {}
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllForCruiseWithError error: Error) {}
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllForUser requests: [Request]) {}
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllForUserWithError error: Error) {}
    func requestFetcher(_ fetcher: RequestFetcher, didCreateRequest request: Request) {}
    func requestFetcher(_ fetcher: RequestFetcher, didFailCreateRequestWithError error: Error) {}
}



fileprivate extension RequestsViewController {
    enum Filter {
        case none
        case favorite
        case users
        
        var title: String {
            switch self {
            case .favorite: return "Favorite cruises"
            case .users: return "My requests"
            case .none: return "None"
            }
        }
    }
}



