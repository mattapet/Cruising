//
//  RequestFetcher.swift
//  Cruising
//
//  Created by Peter Matta on 2/3/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol RequestManagerDelegate: class {
    func requestManager(_ manager: RequestManager, didRemoveRequest request: Request)
    func requestManager(_ manager: RequestManager, didFailRemoveRequestWithError error: Error)
    
    func requestManager(_ manager: RequestManager, didAccepetRequest request: Request)
    func requestManager(_ manager: RequestManager, didFailAcceptRequestWithError error: Error)
}

class RequestManager {
    weak var delegate: RequestManagerDelegate?
    
    func acceptRequest(_ request: Request, withUser user: User) {
        API.RequestAPI.accept(request, with: user) { [unowned self] (_, error) in
            if let error = error {
                self.delegate?.requestManager(self, didFailAcceptRequestWithError: error)
            } else {
                self.delegate?.requestManager(self, didAccepetRequest: request)
            }
        }
    }
    
    func removeRequest(_ request: Request) {
        API.RequestAPI.remove(request) { [unowned self] (error) in
            if let error = error {
                self.delegate?.requestManager(self, didFailRemoveRequestWithError: error)
            } else {
                self.delegate?.requestManager(self, didRemoveRequest: request)
            }
        }
    }
}
