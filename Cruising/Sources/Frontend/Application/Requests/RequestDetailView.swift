//
//  RequestDetailView.swift
//  Cruising
//
//  Created by Peter Matta on 2/6/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

// MARK: - Table view data source procol conformance
extension RequestDetailViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].fields.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].header
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].fields[indexPath.row].height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        let field = section.fields[indexPath.row]
        switch section {
        case .links:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: DefaultCell.reuseIdentifier,
                           for: indexPath
            ) as! DefaultCell
            cell.accessoryType = .disclosureIndicator
            switch field {
            case .cruiseLink: cell.titleLabel.text = "Cruise"
            case .userLink: cell.titleLabel.text = "User"
            default: fatalError("Unknown row")
            }
            return cell
            
        case .manage:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: TitleCell.reuseIdentifier,
                           for: indexPath
            ) as! TitleCell
            cell.selectionStyle = .`default`
            cell.titleLabel.text = actionType.title
            cell.titleLabel.textColor = actionType.color
            return cell
        default: fatalError("Unknown section")
        }
    }
}


// MARK: - Form section and fields
extension RequestDetailViewController {
    enum FormField {
        case cruiseLink
        case userLink
        case manage
        
        var height: CGFloat {
            switch self {
            default: return 44.0
            }
        }
    }
    
    enum FormSection {
        case details
        case links
        case manage
        
        var fields: [FormField] {
            switch self {
            case .details: return []
            case .links: return [.cruiseLink, .userLink]
            case .manage: return [.manage]
            }
        }
        
        var header: String? {
            switch self {
            case .details: return "Details"
            case .links: return "Links"
            default: return nil
            }
        }
    }
}

