//
//  RequestDetailViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/5/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class RequestDetailViewController: UITableViewController {
    internal let sections: [FormSection] = [.links, .manage]
    internal var actionType: ActionType = .accept
    
    private let requestManager = RequestManager()

    var requests: [Request]!
    
    var user: User!
    var request: Request!
    
    
    init() {
        super.init(style: .grouped)
        title = "Request details"
        requestManager.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(
            DefaultCell.self,
            forCellReuseIdentifier: DefaultCell.reuseIdentifier
        )
        tableView.register(
            TitleCell.self,
            forCellReuseIdentifier: TitleCell.reuseIdentifier
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        actionType = user.id == request.user.id ? .remove : .accept
        tableView.reloadData()
    }
    
    
    // MARK: - UI handler
    
    @objc private func onConfirmAction() {
        if actionType == .accept {
            requestManager.acceptRequest(request, withUser: user)
        } else {
            requestManager.removeRequest(request)
        }
    }
}


// MARK: - Table view delegate protocol conformance
extension RequestDetailViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sections[indexPath.section].fields[indexPath.row] == .userLink {
            let profileVC = ProfileViewController()
            profileVC.user = request.user
            navigationController?.pushViewController(profileVC, animated: true)
        }
        if sections[indexPath.section].fields[indexPath.row] == .cruiseLink {
            let cruiseVC = CruiseDetailViewController()
            cruiseVC.cruise = request.cruise
            cruiseVC.user = user
            navigationController?.pushViewController(cruiseVC, animated: true)
        }
        
        if sections[indexPath.section] == .manage {
            let alert = UIAlertController(
                         title: "\(actionType.title) request",
                       message: "Are you sure you want to \(actionType.title.lowercased()) this request?",
                preferredStyle: .alert
            )
            let cancelAction = UIAlertAction(
                title: "Cancel",
                style: .cancel
            )
            let confirmAction = UIAlertAction(
                title: actionType.title,
                style: actionType.style
            ) { [weak self] _ in self?.onConfirmAction() }
            alert.addAction(cancelAction)
            alert.addAction(confirmAction)
            present(alert, animated: true)
        }
    }
}


extension RequestDetailViewController: RequestManagerDelegate {
    func requestManager(_ manager: RequestManager, didRemoveRequest request: Request) {
        requests.remove(at: (requests.index { $0.id == request.id })!)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func requestManager(_ manager: RequestManager, didFailRemoveRequestWithError error: Error) {}
    
    func requestManager(_ manager: RequestManager, didAccepetRequest request: Request) {
        requests.remove(at: (requests.index { $0.id == request.id })!)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func requestManager(_ manager: RequestManager, didFailAcceptRequestWithError error: Error) {}
}



extension RequestDetailViewController {
    enum ActionType {
        case accept
        case remove
        
        var title: String {
            switch self {
            case .accept: return "Accept"
            case .remove: return "Remove"
            }
        }
        
        var style: UIAlertActionStyle {
            switch self {
            case .accept: return .`default`
            case .remove: return .destructive
            }
        }
        
        var color: UIColor {
            switch self {
            case .accept: return .green
            case .remove: return .red
            }
        }
    }
}

