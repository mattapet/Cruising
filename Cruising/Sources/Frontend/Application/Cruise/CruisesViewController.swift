//
//  CruisesViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class CruisesViewController: CruiseListViewController {
    // MARK: - Properties
   
    private let cruiseManager = CruiseManager()
    
   
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(
//            barButtonSystemItem: .search,
//            target: self,
//            action: #selector(onSearchButtonTapped)
//        )
        cruiseManager.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if cruises.count == 0 {
            cruiseManager.fetchAll()
            tableView.refreshControl?.beginRefreshing()
        }
    }
    
    override internal func onRefresh() {
        cruiseManager.fetchAll()
    }
    
    
    // MARK: - Cruise manager delegate protocol conformance
    
    override func cruiseManager(_ manager: CruiseManager, didFetchAllCruises cruises: [Cruise]) {
        cruisesState.cruises = cruises
        tableView.reloadData()
        tableView.refreshControl?.endRefreshing()
    }
    
    override func cruiseManager(_ manager: CruiseManager, didFailFetchAllCrusesWithError error: Error) {
        tableView.refreshControl?.endRefreshing()
    }
}

// MARK: - UI event handlers
extension CruisesViewController {
    @objc private func onSearchButtonTapped() {
        let searchVC = CruiseSearchViewController(user: user, cruisesState: cruisesState)
        present(UINavigationController(rootViewController: searchVC), animated: true)
    }
}
