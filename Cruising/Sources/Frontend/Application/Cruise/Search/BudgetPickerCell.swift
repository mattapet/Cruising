//
//  BudgetPickerCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class BudgetPickerCell: UITableViewCell {
    weak var budgetPicker: BudgetPicker!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let budgetPicker = BudgetPicker()
        contentView.addSubview(budgetPicker)
        budgetPicker.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        self.budgetPicker = budgetPicker
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Class properties
    class var reuseIdentifier: String {
        return "BudgetPickerCell"
    }
}
