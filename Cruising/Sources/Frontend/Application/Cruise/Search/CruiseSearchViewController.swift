//
//  CruiseSearchViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class CruiseSearchViewController: UITableViewController, BudgetPickerDelegate {
    internal let sections: [FormSection] = [.budget]
    
    var user: User
    var cruisesState: CruisesState
    
    // MARK: - UI properties
    
    weak var budgetPicker: BudgetPicker!
    
    
    // MARK: - Initialization
    
    init(user: User, cruisesState: CruisesState) {
        self.user = user
        self.cruisesState = cruisesState
        super.init(style: .grouped)
        title = "Filter cruises"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(
            BudgetPickerCell.self,
            forCellReuseIdentifier: BudgetPickerCell.reuseIdentifier
        )
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(onCancelButtonTapped)
        )
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Next",
            style: .plain,
            target: nil,
            action: nil
        )
        navigationItem.rightBarButtonItem?.isEnabled = false
    }

}

// MARK: - UI handlers
extension CruiseSearchViewController {
    
    @objc fileprivate func onCancelButtonTapped() {
        dismiss(animated: true)
    }
    
    @objc fileprivate func onBudgetChange() {
        print(budgetPicker.minSelected, budgetPicker.maxSelected)
    }
    
    func budgetPickerDidChange(_ picker: BudgetPicker) {
        
    }
}


// MARK: - Table view data source protocol conformance
extension CruiseSearchViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].fields.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].fields[indexPath.row].height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: BudgetPickerCell.reuseIdentifier,
                       for: indexPath
        ) as! BudgetPickerCell
        budgetPicker = cell.budgetPicker
        cell.budgetPicker.delegate = self
        return cell
    }
}


// MARK: - Table view delegate protocol conformance
extension CruiseSearchViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


// MARK: - Static form section and fields
extension CruiseSearchViewController {
    enum FormField {
        case budget
        
        var height: CGFloat {
            switch self {
            default: return 44.0
            }
        }
    }
    
    enum FormSection {
        case budget
        
        var fields: [FormField] {
            switch self {
            case .budget: return [.budget]
            }
        }
        
        var title: String {
            switch self {
            case .budget: return "Budget"
            }
        }
    }
}

