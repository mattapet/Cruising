//
//  ApplicationViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class ApplicationViewController: UITabBarController {
    var appState: ApplicationState
    
    required init(appState: ApplicationState) {
        self.appState = appState
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let cruisesVC = CruisesViewController()
        cruisesVC.cruisesState = appState.cruises
        cruisesVC.user = appState.auth.user
        cruisesVC.tabBarItem = UITabBarItem(
            title: "",
            image: UIImage(named: "cruise_icon_outlined"),
            selectedImage: UIImage(named: "cruise_icon")
        )
        cruisesVC.tabBarItem.tag = 0
        cruisesVC.title = "Cruises"
        
        
        let requestsVC = RequestsViewController()
        requestsVC.user = appState.auth.user
        requestsVC.tabBarItem = UITabBarItem(
            title: "",
            image: UIImage(named: "request_icon_outlined"),
            selectedImage: UIImage(named: "request_icon")
        )
        requestsVC.tabBarItem.tag = 1
        requestsVC.title = "Requests"
        
        
        
        let reservationsVC = ReservationsViewController()
        reservationsVC.user = appState.auth.user
        reservationsVC.tabBarItem = UITabBarItem(
            title: "",
            image: UIImage(named: "reservation_icon_outlined"),
            selectedImage: UIImage(named: "reservation_icon")
        )
        reservationsVC.tabBarItem.tag = 4
        reservationsVC.title = "Reservations"
        
        
        let favoritesVC = FavoritesViewController()
        favoritesVC.cruisesState = appState.cruises
        favoritesVC.user = appState.auth.user
        favoritesVC.tabBarItem = UITabBarItem(
            title: "",
            image: UIImage(named: "heart_icon_outlined"),
            selectedImage: UIImage(named: "heart_icon")
        )
        favoritesVC.tabBarItem.tag = 3
        favoritesVC.title = "Favorites"
        
        
        let profileVC = ProfileViewController()
        profileVC.authState = appState.auth
        profileVC.user = appState.auth.user
        profileVC.tabBarItem = UITabBarItem(
            title: "",
            image: UIImage(named: "profile_icon_outlined"),
            selectedImage: UIImage(named: "profile_icon")
        )
        profileVC.tabBarItem.tag = 4
        profileVC.title = "Profile"
        
        
        let viewControllers = [
            cruisesVC,
            requestsVC,
            reservationsVC,
            favoritesVC,
            profileVC,
        ]
        self.viewControllers = viewControllers.map {
            UINavigationController(rootViewController: $0)
        }
        tabBar.items!.forEach { item in
            item.title = nil
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
    }
}
