//
//  ReservationsViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/6/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class ReservationsViewController: UserCruiseRelatableListViewController {
    private let reservationFetcher = ReservationFetcher()
    private var reservations = [Reservation]()
    
    var user: User!
    
    override var items: [UserCruiseRelatable] {
        return reservations.filter { $0.confirmer.id == user.id || $0.user.id == user.id }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reservationFetcher.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if reservations.count == 0 {
            reservationFetcher.fetchAll()
            tableView.refreshControl?.beginRefreshing()
        }
    }
    
    override func onRefresh() {
        reservationFetcher.fetchAll()
        tableView.refreshControl?.beginRefreshing()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let reservation = reservations[indexPath.section]
        let detailVC = ReservationDetailViewController()
        detailVC.user = user
        detailVC.reservation = reservation
        navigationController?.pushViewController(detailVC, animated: true)
    }
}


extension ReservationsViewController: ReservationFetcherDelegate {
    func reservationFetcher(_ fetcher: ReservationFetcher, didFetchAllReservaitons reservations: [Reservation]) {
        self.reservations = reservations.sorted { $0.id > $1.id }
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    func reservationFetcher(_ fetcher: ReservationFetcher, didFailFetchAllReservaitonsWithError error: Error) {
        tableView.refreshControl?.endRefreshing()
    }
}
