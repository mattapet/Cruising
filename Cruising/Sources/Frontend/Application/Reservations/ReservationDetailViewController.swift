//
//  ReservationDetailViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/9/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class ReservationDetailViewController: UITableViewController {
    private let reservationManager = ReservationManager()
    private let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    internal let sections: [FormSection] = [.links, .status]
    
    var user: User!
    var reservation: Reservation!
    
    init() {
        super.init(style: .grouped)
        title = "Reservation details"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(
            DefaultCell.self,
            forCellReuseIdentifier: DefaultCell.reuseIdentifier
        )
        tableView.register(
            TitleCell.self,
            forCellReuseIdentifier: TitleCell.reuseIdentifier
        )
        reservationManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addActionButton()
        tableView.reloadData()
    }
}

// MARK: UI event handling
extension ReservationDetailViewController {
    private func addActionButton() {
        if let _ = reservation.state.nextStates {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .action,
                target: self,
                action: #selector(onShowActions)
            )
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    private func addActivity() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: spinner)
        spinner.startAnimating()
    }
    
    @objc private func onShowActions() {
        guard let nextStates = reservation.state.nextStates else { return }
        let alert = UIAlertController(
                     title: "Perfom action",
                   message: nil,
            preferredStyle: .actionSheet
        )
        nextStates.forEach { nextState in
            if nextState == .confirmed && reservation.confirmer.id == user.id { return }
            let action = UIAlertAction(
                title: nextState.actionTitle,
                style: nextState.isDestructive ? .destructive : .`default`
            ) { [unowned self] _ in self.performStateChange(nextState) }
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "None", style: .cancel))
        present(alert, animated: true)
    }
    
    private func performStateChange(_ nextState: Reservation.State) {
        addActivity()
        reservationManager.updateState(nextState, for: reservation)
    }
}


// MARK: Table view delegate protocol conformance
extension ReservationDetailViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sections[indexPath.section].fields[indexPath.row] == .userLink {
            let profileVC = ProfileViewController()
            profileVC.user = reservation.user.id == user.id ? reservation.confirmer : reservation.user
            navigationController?.pushViewController(profileVC, animated: true)
        }
        if sections[indexPath.section].fields[indexPath.row] == .cruiseLink {
            let cruiseVC = CruiseDetailViewController()
            cruiseVC.cruise = reservation.cruise
            cruiseVC.user = user
            navigationController?.pushViewController(cruiseVC, animated: true)
        }
    }
}


// MARK: - Reservation manager delegate protocol conformance
extension ReservationDetailViewController: ReservationManagerDelegate {
    func reservationManager(_ manager: ReservationManager, didUpdateStateForReservation reservation: Reservation) {
        self.reservation.state = reservation.state
        addActionButton()
        tableView.reloadData()
    }
    
    func reservationManager(_ manager: ReservationManager, didFailUpdateStateWithError error: Error) {
        addActionButton()
        tableView.reloadData()
    }
}


