//
//  ReservationFetcher.swift
//  Cruising
//
//  Created by Peter Matta on 2/6/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol ReservationFetcherDelegate: class {
    func reservationFetcher(_ fetcher: ReservationFetcher, didFetchAllReservaitons reservations: [Reservation])
    func reservationFetcher(_ fetcher: ReservationFetcher, didFailFetchAllReservaitonsWithError error: Error)
}

class ReservationFetcher {
    weak var delegate: ReservationFetcherDelegate?
    
    func fetchAll() {
        API.ReservationAPI.fetchAll { [unowned self] (reservations, error) in
            if let error = error {
                self.delegate?.reservationFetcher(self, didFailFetchAllReservaitonsWithError: error)
            } else {
                self.delegate?.reservationFetcher(self, didFetchAllReservaitons: reservations!)
            }
        }
    }
}
