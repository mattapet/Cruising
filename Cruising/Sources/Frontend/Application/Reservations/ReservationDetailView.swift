//
//  ReservationDetailView.swift
//  Cruising
//
//  Created by Peter Matta on 2/9/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

// MARK: - Table view data source protocol conformance
extension ReservationDetailViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].fields.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].header
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].fields[indexPath.row].height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        let field = section.fields[indexPath.row]
        switch section {
        case .links:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: DefaultCell.reuseIdentifier,
                           for: indexPath
            ) as! DefaultCell
            cell.accessoryType = .disclosureIndicator
            cell.titleLabel.text = field == .userLink ? "User" : "Cruise"
            return cell
        case .status:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: TitleCell.reuseIdentifier,
                           for: indexPath
            ) as! TitleCell
            let state = reservation.state
            cell.titleLabel.text = state.title
            cell.titleLabel.textColor = state.isDestructive ? .red : .black
            return cell
        default: fatalError("Unknown section")
        }
    }
}

extension ReservationDetailViewController {
    enum FormField {
        case cruiseLink
        case userLink
        case status
        
        var height: CGFloat {
            switch self {
            default: return 44.0
            }
        }
    }
    
    enum FormSection {
        case details
        case links
        case status
        
        var fields: [FormField] {
            switch self {
            case .details: return[]
            case .links: return [.cruiseLink, .userLink]
            case .status: return [.status]
            }
        }
        
        var header: String? {
            switch self {
            case .details: return "Details"
            case .links: return "Links"
            case .status: return "Current status"
            }
        }
    }
}
