//
//  FavoritesViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class FavoritesViewController: CruiseListViewController {
    private let cruiseManager = CruiseManager()

    override var cruises: [Cruise] {
        return cruisesState.cruises.filter { user.favoriteIds.contains($0.id) }
    }
    
    
    // MARK: View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cruiseManager.delegate = self
    }
    
    override internal func onRefresh() {
        cruiseManager.fetchFavorites(for: user)
    }
    
    
    // MARK: - Cruise manager delegate protocol conformance
    
    override func cruiseManager(_ manager: CruiseManager, didFetchAllFavorites favorites: [Cruise]) {
        let favoriteIds = favorites.map { $0.id }
        user.favoriteIds = Set(favoriteIds)
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
}
