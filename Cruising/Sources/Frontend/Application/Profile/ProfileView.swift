//
//  ProfileView.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

// MARK: - Table view data source protocol conformance
extension ProfileViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].fields.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let formField = sections[indexPath.section].fields[indexPath.row]
        return formField.height
    }
}

// MARK: - Cell rendering
extension ProfileViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let formField = sections[indexPath.section].fields[indexPath.row]
        switch formField {
        case .profilePicture: return profilePictureCell(indexPath)
        case .name: return nameCell(indexPath)
        default: return detailCell(indexPath, for: formField)
        }
    }
    
    private func profilePictureCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ProfilePictureCell.reuseIdentifier,
            for: indexPath
        ) as! ProfilePictureCell
        cell.update(user)
        return cell
    }
    
    private func nameCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: TitleCell.reuseIdentifier,
            for: indexPath
        ) as! TitleCell
        cell.titleLabel.text = user.fullName
        return cell
    }
    
    private func detailCell(_ indexPath: IndexPath, `for` field: FormField) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: DetailCell.reuseIdentifier,
            for: indexPath
        ) as! DetailCell
        
        switch field {
        case .email:
            cell.nameLabel.text = "Email"
            cell.valueLabel.attributedText = NSAttributedString(
                    string: user.email,
                attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
            )
            cell.selectionStyle = .`default`
            
        case .age:
            cell.nameLabel.text = "Age"
            cell.valueLabel.text = "\(user.age)"
        case .gender:
            cell.nameLabel.text = "Gender"
            cell.valueLabel.text = user.gender.rawValue
        default: fatalError("unknown form field")
        }
        return cell
    }
}

// MARK: - Form fields
extension ProfileViewController {
    enum FormField {
        case profilePicture
        case name
        case email
        case age
        case gender
        
        var height: CGFloat {
            switch self {
            case .profilePicture: return 120.0
            case .name: return 66.0
            default: return 44.0
            }
        }
    }
    
    enum FormSection {
        case main
        
        var fields: [FormField] {
            return [.profilePicture, .name, .email, .age, .gender]
        }
    }
}
