//
//  ProfilePictureCell.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class ProfilePictureCell: UITableViewCell {
    weak var profilePicture: ProfilePictureView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        let profilePicture = ProfilePictureView(gender: .male)
        contentView.addSubview(profilePicture)
        profilePicture.snp.makeConstraints { make in
            make.height.equalToSuperview()
            make.width.equalTo(profilePicture.snp.height)
            make.center.equalToSuperview()
        }
        self.profilePicture = profilePicture
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ user: User) {
        profilePicture.gender = user.gender
    }
    
    class var reuseIdentifier: String {
        return "ProfilePictureCell"
    }
}
