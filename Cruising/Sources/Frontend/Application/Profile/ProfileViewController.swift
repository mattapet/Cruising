//
//  ProfileViewController.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    private let logoutManager = LogoutManager()
    internal let sections: [FormSection] = [.main]
    
    // MARK: - Dependency injection
    
    var authState: AuthorizationState?
    var user: User!
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoutManager.delegate = self
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(ProfilePictureCell.self, forCellReuseIdentifier: ProfilePictureCell.reuseIdentifier)
        tableView.register(TitleCell.self, forCellReuseIdentifier: TitleCell.reuseIdentifier)
        tableView.register(DetailCell.self, forCellReuseIdentifier: DetailCell.reuseIdentifier)
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        navigationItem.title = user.fullName
        if authState != nil {
            addLogoutButton()
        } else {
            removeLogoutButton()
        }
    }
    
}

// MARK: - Convenience private methods
fileprivate extension ProfileViewController {
    func translateIndexPath(_ indexPath: IndexPath) -> (FormSection, FormField) {
        let formSection = sections[indexPath.section]
        let formField = formSection.fields[indexPath.row]
        return (formSection, formField)
    }
    
    @objc func logoutButtonTapped() {
        let actionVC = UIAlertController(
                     title: "Log out?",
                   message: "Do you want to log out?",
            preferredStyle: .actionSheet
        )
        let logoutAction = UIAlertAction(
            title: "Log out",
            style: .destructive) { [unowned self] _ in
            self.logoutManager.logout()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        actionVC.addAction(logoutAction)
        actionVC.addAction(cancelAction)
        present(actionVC, animated: true)
    }
    
    func addLogoutButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
             title: "Log out",
             style: .plain,
            target: self,
            action: #selector(logoutButtonTapped)
        )
    }
    
    func removeLogoutButton() {
        navigationItem.rightBarButtonItem = nil
    }
}


// MARK: - Authorization manager delegate protocol conformance
extension ProfileViewController: LogoutManagerDelegate {
    func logoutManagerDidLogoutUser(_ manager: LogoutManager) {
        authState?.user = nil
        authState?.store()
        tabBarController?.dismiss(animated: true)
    }
    
    func logoutManager(_ manager: LogoutManager, didFailLogoutUserWithError error: Error) { }
}


// MARK: - Table view delegate protocol conformance source
extension ProfileViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sections[indexPath.section].fields[indexPath.row] == .email {
            let mailUrl = URL(string: "mailto:\(user.email)")!
            UIApplication.shared.canOpenURL(mailUrl)
            UIApplication.shared.open(mailUrl)
        }
    }
}


