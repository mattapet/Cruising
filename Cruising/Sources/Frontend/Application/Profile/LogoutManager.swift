//
//  LogoutManager.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import Foundation

protocol LogoutManagerDelegate: class {
    func logoutManagerDidLogoutUser(_ manager: LogoutManager)
    func logoutManager(_ manager: LogoutManager, didFailLogoutUserWithError error: Error)
}

class LogoutManager {
    weak var delegate: LogoutManagerDelegate?
    
    func logout() {
        API.AuthorizationAPI.logout { [unowned self] (error) in
            if let error = error {
                self.delegate?.logoutManager(self, didFailLogoutUserWithError: error)
            } else {
                self.delegate?.logoutManagerDidLogoutUser(self)
            }
        }
    }
}
