//
//  CruiseCell.swift
//  Cruising
//
//  Created by Peter Matta on 1/31/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class CruiseCell: UITableViewCell {
    // MARK: - UI properties
    weak var detailContainer: UIStackView!
    weak var coverImage: URLImageView!
    weak var nameLabel: UILabel!
    weak var priceLabel: UILabel!
    
    
    // MARK: - Initialization
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let coverImage = URLImageView()
        contentView.addSubview(coverImage)
        coverImage.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-36)
        }
        self.coverImage = coverImage
        
        
        let nameLabel = UILabel()
        let priceLabel = UILabel()
        priceLabel.textAlignment = .right
        
        let detailContainer = UIStackView(arrangedSubviews: [
            nameLabel, priceLabel
        ])
        contentView.addSubview(detailContainer)
        detailContainer.axis = .horizontal
        detailContainer.alignment = .fill
        detailContainer.distribution = .fillProportionally

        detailContainer.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.centerX.bottom.equalToSuperview()
            make.top.equalTo(coverImage.snp.bottom)
        }
        self.detailContainer = detailContainer
        
        
        self.nameLabel = nameLabel
        self.priceLabel = priceLabel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Content Update
    
    func update(_ cruise: Cruise) {
        coverImage.url = cruise.coverImageURL
        nameLabel.text = cruise.name
        priceLabel.text = Formatters.currencyFormatter.string(from: NSNumber(integerLiteral: cruise.price))
    }
    
    
    // MARK: - Class properties
    
    class var reuseIdentifier: String {
        return "CruiseCell"
    }
}
