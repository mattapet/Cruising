//
//  CruiseListViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class CruiseListViewController: UITableViewController, CruiseManagerDelegate {
    // MARK: - Dependency injection
    var user: User!
    var cruisesState: CruisesState!
    var cruises: [Cruise] {
        return cruisesState.cruises
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .groupTableViewBackground
        tableView.tableFooterView = UIView()
        setupRefreshControl()
        setupReusableCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.refreshControl?.endRefreshing()
    }
    
    
    // MARK: - Setup methods
    
    internal func setupRefreshControl() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(
            self,
            action: #selector(onRefresh),
               for: .valueChanged
        )
    }
    
    internal func setupReusableCells() {
        tableView.register(
            CruiseCell.self,
            forCellReuseIdentifier: CruiseCell.reuseIdentifier
        )
    }
    
    @objc internal func onRefresh() { }
    
    
    // MARK: - Cruise manager delegate protocol conformance
    
    func cruiseManagerDidUpdate(_ manager: CruiseManager) {
        tableView.reloadData()
    }
    func cruiseManager(_ manager: CruiseManager, didFetchAllCruises cruises: [Cruise]) { }
    func cruiseManager(_ manager: CruiseManager, didFailFetchAllCrusesWithError error: Error) { }
    func cruiseManager(_ manager: CruiseManager, didFetchAllFavorites favorites: [Cruise]) { }
    func cruiseManager(_ manager: CruiseManager, didFailFetchAllFavoritesWithError error: Error) { }
    func cruiseManager(_ manager: CruiseManager, didAddFavoriteCruise cruise: Cruise) { }
}


// MARK: Table view delegate protocol conformance
extension CruiseListViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selected = cruises[indexPath.section]
        let detailVC = CruiseDetailViewController()
        detailVC.cruise = selected
        detailVC.user = user
        navigationController?.pushViewController(detailVC, animated: true)
    }
}


// MARK: - Table view data source protocol conformance
extension CruiseListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return cruises.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 : 10.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cruise = cruises[indexPath.section]
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CruiseCell.reuseIdentifier,
                       for: indexPath
        ) as! CruiseCell
        cell.update(cruise)
        return cell
    }
}


