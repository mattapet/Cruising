//
//  CruiseDetailViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class CruiseDetailViewController: UITableViewController, FavoriteManagerDelegate {
    private let favoriteManager = FavoriteManager()
    internal let sections: [FormSection] = [.images, .details, .description, .requests]
    var user: User!
    var cruise: Cruise!
    
    var isFavorite: Bool {
        return user.favoriteIds.contains(cruise.id)
    }
    
    // MARK: - initialization
    
    convenience init() {
        self.init(style: .grouped)
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteManager.delegate = self
        tableView.register(
            DetailCell.self,
            forCellReuseIdentifier: DetailCell.reuseIdentifier
        )
        tableView.register(
            DefaultCell.self,
            forCellReuseIdentifier: DefaultCell.reuseIdentifier
        )
        tableView.register(
            SlideshowCell.self,
            forCellReuseIdentifier: SlideshowCell.reuseIdentifier
        )
        tableView.contentInset = UIEdgeInsets(top: -35, left: 0, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = cruise.name
        addFavoriteButton()
        tableView.reloadData()
    }
    
    
    // MARK: - UI handlers
    
    @objc func onFavoriteButtonTapped() {
        disableFavorite()
        if isFavorite {
            favoriteManager.removeFavorite(cruise, from: user)
        } else {
            favoriteManager.addFavorite(cruise, to: user)
        }
    }
    
    func fetchImagesIfNeeded(`for` cruise: Cruise) {
        if cruise.images == nil {
            var images: [Data] = []
            let dispatchGroup = DispatchGroup()
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                cruise.imageURLs.forEach { url in
                    dispatchGroup.enter()
                    let session = URLSession(configuration: .ephemeral)
                    let task = session.dataTask(with: url) { (data, _, _) in
                        dispatchGroup.leave()
                        guard let imageData = data else { print("returning"); return }
                        images.append(imageData)
                    }
                    task.resume()
                }
                
                dispatchGroup.wait()
                DispatchQueue.main.async { [weak self] in
                    cruise.images = images
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Table view delegate protocol conformance
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sections[indexPath.section].fields[indexPath.row] == .requests {
            let requestsVC = CruiseRequestsViewController()
            requestsVC.cruise = cruise
            requestsVC.user = user
            navigationController?.pushViewController(requestsVC, animated: true)
        }
    }
    
    // MARK: - Favorite manager delegate protocol conformance
    
    func favoriteManager(_ manager: FavoriteManager, didAddFavoriteCruise cruise: Cruise) {
        user.favoriteIds.insert(cruise.id)
        addFavoriteButton()
    }
    
    func favoriteManager(_ manager: FavoriteManager, didFailAddFavoriteCruiseWithError error: Error) {
        addFavoriteButton()
    }
    
    func favoriteManager(_ manager: FavoriteManager, didRemoveFavoriteCruise cruise: Cruise) {
        user.favoriteIds.remove(cruise.id)
        addFavoriteButton()
    }
    
    func favoriteManager(_ manager: FavoriteManager, didFailRemoveFavoriteCruiseWithError error: Error) {
        addFavoriteButton()
    }
}


// MARK: - Helper methods
fileprivate extension CruiseDetailViewController {
    func addFavoriteButton() {
        let imageName = "heart_icon\(isFavorite ? "" : "_outlined")"
        let image = UIImage(named: imageName)
        let favoriteButton = UIBarButtonItem(
             image: image,
             style: .plain,
            target: self,
            action: #selector(onFavoriteButtonTapped)
        )
        navigationItem.rightBarButtonItem = favoriteButton
    }
    
    func disableFavorite() {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let spinnerItem = UIBarButtonItem(customView: spinner)
        spinner.startAnimating()
        navigationItem.rightBarButtonItem = spinnerItem
    }
}

// MARK: - Static form sections and fields
extension CruiseDetailViewController {
    enum FormField {
        case coverImage
        case name
        case price
        case description
        case requests
        case images
        
        var height: CGFloat {
            switch self {
            case .images: return 200.0
            case .description: return UITableViewAutomaticDimension
            default: return 44.0
            }
        }
    }
    
    enum FormSection {
        case details
        case description
        case requests
        case images
        
        var fields: [FormField] {
            switch self {
            case .details: return [.name, .price]
            case .description: return [.description]
            case .requests: return [.requests]
            case .images: return [.images]
            }
        }
        
        var header: String? {
            switch self {
            case .details: return "Details"
            case .description: return "Description"
            default: return nil
            }
        }
    }
}
