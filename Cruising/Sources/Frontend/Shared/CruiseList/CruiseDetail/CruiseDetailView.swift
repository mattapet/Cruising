//
//  CruiseDetailView.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit


// MARK: - Table view data source
extension CruiseDetailViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].fields.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].header
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].fields[indexPath.row].height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        
        switch section {
        case .details: return detailCellForRowAt(indexPath)
        case .description: return descriptionCellForRowAtIndexPath(indexPath)
        case .requests: return requestsCellForRowAt(indexPath)
        case .images: return slideshowCellForRowAt(indexPath)
        }
    }
}

fileprivate extension CruiseDetailViewController {
    
    func detailCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let field = sections[indexPath.section].fields[indexPath.row]
        let cell = tableView.dequeueReusableCell(
            withIdentifier: DetailCell.reuseIdentifier,
                       for: indexPath
        ) as! DetailCell
        switch field {
        case .name:
            cell.nameLabel.text = "Name"
            cell.valueLabel.text = cruise.name
        case .price:
            cell.nameLabel.text = "Price"
            cell.valueLabel.text = Formatters.currencyFormatter.string(from: NSNumber(integerLiteral: cruise.price))
        default:
            fatalError("Unknown field")
        }
        return cell
    }
    
    func descriptionCellForRowAtIndexPath(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: DefaultCell.reuseIdentifier,
                       for: indexPath
        ) as! DefaultCell
        cell.titleLabel.text = cruise.cruiseDescription ?? "No description"
        cell.titleLabel.numberOfLines = 0
        cell.selectionStyle = .none
        cell.accessoryType = .none
        return cell
    }
    
    func requestsCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: DefaultCell.reuseIdentifier,
                       for: indexPath
        ) as! DefaultCell
        cell.titleLabel.text = "Requests"
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .`default`
        return cell
    }
    
    func slideshowCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: SlideshowCell.reuseIdentifier,
                       for: indexPath
        ) as! SlideshowCell
        fetchImagesIfNeeded(for: cruise)
        cell.update(cruise.images)
        return cell
    }
}

