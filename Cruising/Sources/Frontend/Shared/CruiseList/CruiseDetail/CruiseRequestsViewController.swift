//
//  CruiseRequestsViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/2/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class CruiseRequestsViewController: UserListViewController {
    private let requestManager = RequestFetcher()
    var user: User!
    var cruise: Cruise!
    
    override var items: [User] {
        return cruise.requests?
            .map { $0.user }
            .filter { $0.id != user.id } ?? []
    }
    
    var canRequest: Bool {
        return !(cruise.requests?.contains { $0.user.id == user.id } ?? false)
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Requests"
        requestManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if cruise.requests == nil {
            tableView.refreshControl?.beginRefreshing()
            requestManager.fetchAll(for: cruise)
        }
        resolveCreateButton()
    }
    
    private func resolveCreateButton() {
        if canRequest {
            addCreateButton()
        } else {
            removeCreateButton()
        }
    }
    
    // MARK: - UI handlers
    
    @objc override func onRefresh() {
        tableView.refreshControl?.beginRefreshing()
        requestManager.fetchAll(for: cruise)
    }
    
    @objc func onAddRequestTapped() {
        addSpinner()
        requestManager.createRequest(for: cruise, with: user)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = ProfileViewController()
        profileVC.user = itemFor(indexPath)
        navigationController?.pushViewController(profileVC, animated: true)
    }
}


// MARK: - Add button handling
fileprivate extension CruiseRequestsViewController {
    func addCreateButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(onAddRequestTapped)
        )
    }
    
    func removeCreateButton() {
        navigationItem.rightBarButtonItem = nil
    }
    
    func addSpinner() {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: spinner)
    }
}


// MARK: - Request manager delegate protocol conformance
extension CruiseRequestsViewController: RequestFetcherDelegate {
    func requestFetcher(_ fetcher: RequestFetcher, didFetchAllRequests requests: [Request]) {}
    func requestFetcher(_ fetcher: RequestFetcher, didFailFetchAllRequestsWithError error: Error) {}
    
    func requestFetcher(_ manager: RequestFetcher, didCreateRequest request: Request) {
        cruise.requests?.append(request)
        removeCreateButton()
    }
    
    func requestFetcher(_ manager: RequestFetcher, didFailCreateRequestWithError error: Error) {
        addCreateButton()
    }
    
    func requestFetcher(_ manager: RequestFetcher, didFetchAllForCruise requests: [Request]) {
        cruise.requests = requests
        resolveCreateButton()
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    func requestFetcher(_ manager: RequestFetcher, didFailFetchAllForCruiseWithError error: Error) {
        tableView.refreshControl?.endRefreshing()
    }
    
    func requestFetcher(_ manager: RequestFetcher, didFetchAllForUser requests: [Request]) {}
    func requestFetcher(_ manager: RequestFetcher, didFailFetchAllForUserWithError error: Error) {}
}

