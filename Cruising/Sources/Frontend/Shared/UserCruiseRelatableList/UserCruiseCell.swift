//
//  UserCruiseCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/3/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class UserCruiseCell: GenericTableViewCell<UserCruiseRelatable> {
    // MARK: - UI properties
    weak var detailContainer: UIStackView!
    weak var coverImage: URLImageView!
    weak var nameLabel: UILabel!
    weak var priceLabel: UILabel!
    weak var profilePicture: ProfilePictureView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let coverImage = URLImageView()
        contentView.addSubview(coverImage)
        coverImage.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-36)
        }
        self.coverImage = coverImage
        
        
        let nameLabel = UILabel()
        let priceLabel = UILabel()
        priceLabel.textAlignment = .right
        
        let detailContainer = UIStackView(arrangedSubviews: [
            nameLabel, priceLabel
        ])
        contentView.addSubview(detailContainer)
        detailContainer.axis = .horizontal
        detailContainer.alignment = .fill
        detailContainer.distribution = .fillProportionally
        
        detailContainer.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.centerX.bottom.equalToSuperview()
            make.top.equalTo(coverImage.snp.bottom)
        }
        self.detailContainer = detailContainer
        
        
        self.nameLabel = nameLabel
        self.priceLabel = priceLabel
        
        let profilePicture = ProfilePictureView()
        coverImage.addSubview(profilePicture)
        profilePicture.snp.makeConstraints { make in
            make.bottom.trailing.equalToSuperview()
            make.height.equalTo(25)
            make.width.equalTo(profilePicture.snp.height)
        }
        coverImage.bringSubview(toFront: profilePicture)
        self.profilePicture = profilePicture
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update() {
        coverImage.url = item.cruise.coverImageURL
        nameLabel.text = item.cruise.name
        priceLabel.text = Formatters.currencyFormatter.string(from: NSNumber(integerLiteral: item.cruise.price))
        profilePicture.gender = item.user.gender
    }
    
    override class var reuseIdentifier: String {
        return "UserCruiseCell"
    }
}
