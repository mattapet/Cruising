//
//  UserCruiseRelatableListViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/3/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class UserCruiseRelatableListViewController: GenericTableViewController<UserCruiseRelatable, UserCruiseCell> {
    required init() {
        super.init(nibName: nil, bundle: nil)
        displayStyle = .sections
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .groupTableViewBackground
        tableView.tableFooterView = UIView()
    }


    // MARK: - Table view data source protocol conformance
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 10.0
    }
}
