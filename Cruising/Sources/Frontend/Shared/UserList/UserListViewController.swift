//
//  UserListViewController.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit

class UserListViewController: GenericTableViewController<User, UserCell>, UserManagerDelegate {

    // MARK: - Data properties
    
    override var items: [User] {
        return []
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayStyle = .sections
        tableView.backgroundColor = .groupTableViewBackground
    }
    
    // MARK: - User manager delegate protocol conformance
    
    func userManager(_ manager: UserManager, didUpdateUsers users: [User]) { }
    func userManager(_ manager: UserManager, didFetchAllUsers users: [User]) { }
    func userManager(_ manager: UserManager, didFailFetchAllUsersWithError error: Error) { }


    // MARK: - Table view data source protocol conformance

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 10.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

