//
//  UserCell.swift
//  Cruising
//
//  Created by Peter Matta on 2/1/18.
//  Copyright © 2018 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class UserCell: GenericTableViewCell<User> {
    // MARK: - UI properties
    
    weak var container: UIStackView!
    weak var detailContainer: UIStackView!
    
    weak var profilePictureView: ProfilePictureView!
    weak var nameLabel: UILabel!
    weak var ageLabel: UILabel!
    
    
    // MARK: - Initialization
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        let profilePictureView = ProfilePictureView()
        let nameLabel = UILabel()
        let ageLabel = UILabel()
        
        let detailContainer = UIStackView(arrangedSubviews: [
            nameLabel, ageLabel
        ])
        detailContainer.axis = .vertical
        detailContainer.alignment = .center
        detailContainer.distribution = .fillEqually
        detailContainer.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel.textAlignment = .right
        ageLabel.textAlignment = .right
        nameLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
        }
        ageLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
        }
        
        let container = UIStackView(arrangedSubviews: [
            profilePictureView, detailContainer
        ])
        contentView.addSubview(container)
        container.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        container.axis = .horizontal
        container.distribution = .fill
        container.translatesAutoresizingMaskIntoConstraints = false
        self.container = container
        
        self.detailContainer = detailContainer
        
        profilePictureView.snp.makeConstraints { make in
            make.height.leading.equalToSuperview()
            make.width.equalTo(profilePictureView.snp.height)
        }
        self.profilePictureView = profilePictureView
        self.nameLabel = nameLabel
        self.ageLabel = ageLabel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Content update
    
    override func update() {
        profilePictureView.gender = item.gender
        nameLabel.text = item.fullName
        ageLabel.text = "\(item.age)"
    }
    
    // MARK: - Class properties
    
    override class var reuseIdentifier: String {
        return "UserCell"
    }
}
